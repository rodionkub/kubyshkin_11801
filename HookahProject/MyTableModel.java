package HookahProject;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel {
    private String[][] data;

    public MyTableModel(String[][] tableData, String[] colNames) {
        super(tableData, colNames);
        this.data = tableData;
    }

    public boolean isCellEditable(int row, int column) {
        if (column == 0) {
            return false;
        }
        else {
            return true;
        }
    }
}
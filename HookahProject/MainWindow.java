package HookahProject;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainWindow extends JFrame {
    public static JButton sumButton = new JButton("Итог дня");
    JLabel sumLabel = new JLabel("<html><b>Сумма за день: </b>0");
    JLabel hookahLabel = new JLabel("<html><b>Кальянов: </b><html>");
    JLabel teaLabel = new JLabel("<html><b>Чаёв: </b></html>");
    JLabel colaLabel = new JLabel("<html><b>Колы: </b><html>");
    JLabel spriteLabel = new JLabel("<html><b>Спрайта: </b></html>");
    JLabel fantaLabel = new JLabel("<html><b>Фанты: </b><html>");
    JLabel stuffLabel = new JLabel("<html><b>Стафчика: </b></html>");
    JLabel hookahCountLabel = new JLabel("0");
    JLabel teaCountLabel = new JLabel("0");
    JLabel colaCountLabel = new JLabel("0");
    JLabel spriteCountLabel = new JLabel("0");
    JLabel fantaCountLabel = new JLabel("0");
    JLabel stuffCountLabel = new JLabel("0");
    public static JTable table;
    public static String[][] data = new String[8][14];
    public static MyTableModel model;
    public static final String[] days = {
            " ",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"
    };
    public static final String[] months = {
            " ",
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Март",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
    };
    public static final String[] years = {
            " ",
            "2018",
            "2019",
            "2020",
            "2021"
    };
    JComboBox dayComboBox = new JComboBox(days);
    JComboBox monthComboBox = new JComboBox(months);
    JComboBox yearComboBox = new JComboBox(years);
    static Calendar cal = Calendar.getInstance();
    public static int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
    public static int monthInt = cal.get(Calendar.MONTH) + 1;
    public static int year = cal.get(Calendar.YEAR);
    JButton okButton = new JButton("Показать таблицу за эту дату");
    JPanel tablePanel = new JPanel();
    JPanel analysisPanel = new AnalysisPanel();
    JScrollPane scrollPane;
    String[] columnNames;
    String[] rowNames;




    public MainWindow() {
        super("Hookah, motherfucker. Do you speak it?");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);

        setComboBoxes();
        setColumnAndRowNames();

        sumButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                invokeSummarizeButton();
            }
        });
        setOkButton();
        setTable();

        addAllTheShit();
        tabbedPane.add("Table", tablePanel);
        tabbedPane.add("Analysis", analysisPanel);
        tabbedPane.setSize(1300, 800);
        add(tabbedPane);
        setSize(1300,800);
    }

    public void setComboBoxes() {
        dayComboBox.setSelectedIndex(dayOfMonth);
        monthComboBox.setSelectedIndex(monthInt);
        yearComboBox.setSelectedItem(Integer.toString(year));

        dayComboBox.setSize(new Dimension(30, 30));
        monthComboBox.setSize(new Dimension(80, 30));
        yearComboBox.setSize(new Dimension(60, 30));
    }

    public void setColumnAndRowNames() {
        columnNames = new String[]{
                " ",
                "13:00",
                "14:00",
                "15:00",
                "16:00",
                "17:00",
                "18:00",
                "19:00",
                "20:00",
                "21:00",
                "22:00",
                "23:00",
                "00:00",
                "01:00"
        };

        rowNames = new String[]{
                "<html> <center> 1 </center> TABLE",
                "<html> <center> 2 </center> TABLE",
                "<html> <center> 3 </center> TABLE",
                "<html> <center> 4 </center> TABLE",
                "<html> <center> 5 </center> TABLE",
                "<html> <center> 6 </center> TABLE",
                "<html> <center> 7 </center> TABLE",
                "<html> <center> 8 </center> TABLE"
        };
    }

    public void addAllTheShit() {
        JPanel flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel flow2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        tablePanel.add(dayComboBox);
        tablePanel.add(monthComboBox);
        tablePanel.add(yearComboBox);
        tablePanel.add(okButton);
        tablePanel.add(scrollPane);
        tablePanel.add(hookahLabel);
        flow.add(hookahCountLabel);
        flow.add(teaLabel);
        flow.add(teaCountLabel);
        flow.add(colaLabel);
        flow.add(colaCountLabel);
        flow.add(spriteLabel);
        flow.add(spriteCountLabel);
        flow.add(fantaLabel);
        flow.add(fantaCountLabel);
        flow.add(stuffLabel);
        flow.add(stuffCountLabel);
        flow2.add(sumButton);
        flow2.add(sumLabel);
        tablePanel.add(flow);
        tablePanel.add(flow2);
    }

    public void setOkButton() {
        okButton.setSize(new Dimension(30, 30));
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dayOfMonth = Integer.parseInt(dayComboBox.getSelectedItem().toString());
                monthInt = monthComboBox.getSelectedIndex();
                year = Integer.parseInt(yearComboBox.getSelectedItem().toString());

                Scanner sc = null;
                try {
                    sc = new Scanner(new File("data"));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }

                boolean dateFound = false;

                while (sc.hasNextLine()) {
                    String[] line = sc.nextLine().split("\\.");
                    int dataDay = Integer.parseInt(line[0]);
                    int dataMonth = Integer.parseInt(line[1]);
                    int dataYear = Integer.parseInt(line[2]);

                    if (dataDay == dayOfMonth && dataMonth == monthInt && dataYear == year) {
                        dateFound = true;
                        String[] dataRow;
                        String[] dataCell;
                        dataRow = line[3].split(";");
                        for (int i = 0; i < data.length; i++) {
                            for (int j = 1; j < data[0].length; j++) {
                                dataCell = dataRow[i].split(",");
                                if (!dataCell[j - 1].equals("null") && !dataCell[j - 1].equals(" ")) {
                                    table.setValueAt(dataCell[j - 1], i, j);
                                    data[i][j] = dataCell[j - 1];
                                } else {
                                    table.setValueAt(" ", i, j);
                                    data[i][j] = " ";
                                }
                            }
                        }
                    }
                    if (!dateFound) {
                        for (int i = 0; i < data.length; i++) {
                            for (int j = 1; j < data[0].length; j++) {
                                table.setValueAt(" ", i, j);
                                data[i][j] = " ";
                            }
                        }
                    }
                }
                sumButton.doClick();
            }
        });
    }

    public void setTable() {
        for (int i = 0; i < data.length; i++) {
            data[i][0] = rowNames[i];
        }
        model = new MyTableModel(data, columnNames);
        table = new JTable(model);
        table.setGridColor(Color.BLACK);
        table.setRowHeight(75);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        scrollPane = new JScrollPane(table);

        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable) e.getSource();
                int row = target.getSelectedRow();
                int column = target.getSelectedColumn();
                String text = "";
                if (data[row][column] != null) {
                    text = data[row][column];
                }
                if (column != 0) {
                    JFrame checkWindow = new CheckWindow(text, row, column);
                    checkWindow.setVisible(true);
                }
            }
        });
        table.setPreferredSize(new Dimension(1300, 620));
        scrollPane.setPreferredSize(new Dimension(1300, 620));
    }

    public void invokeSummarizeButton() {
        Pattern hookahPattern = Pattern.compile("Кальян x[1-9]");
        Matcher hookahMatcher;
        Pattern teaPattern = Pattern.compile("Чай x[1-9]");
        Matcher teaMatcher;
        Pattern colaPattern = Pattern.compile("Кола x[1-9]");
        Matcher colaMatcher;
        Pattern spritePattern = Pattern.compile("Спрайт x[1-9]");
        Matcher spriteMatcher;
        Pattern fantaPattern = Pattern.compile("Фанта x[1-9]");
        Matcher fantaMatcher;
        Pattern stuffPattern = Pattern.compile("Стафчик x[1-9]");
        Matcher stuffMatcher;
        int hookahCount = 0;
        int teaCount = 0;
        int colaCount = 0;
        int spriteCount = 0;
        int fantaCount = 0;
        int stuffCount = 0;

        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[0].length; col++) {
                if (data[row][col] != null) {
                    hookahMatcher = hookahPattern.matcher(data[row][col]);
                    teaMatcher = teaPattern.matcher(data[row][col]);
                    colaMatcher = colaPattern.matcher(data[row][col]);
                    spriteMatcher = spritePattern.matcher(data[row][col]);
                    fantaMatcher = fantaPattern.matcher(data[row][col]);
                    stuffMatcher = stuffPattern.matcher(data[row][col]);
                    

                    if (hookahMatcher.find()) {
                        String text = data[row][col].substring(hookahMatcher.start(), hookahMatcher.end());
                        int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                        hookahCount += amount;
                    }
                    if (teaMatcher.find()) {
                        String text = data[row][col].substring(teaMatcher.start(), teaMatcher.end());
                        int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                        teaCount += amount;
                    }
                    if (colaMatcher.find()) {
                        String text = data[row][col].substring(colaMatcher.start(), colaMatcher.end());
                        int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                        colaCount += amount;
                    }
                    if (spriteMatcher.find()) {
                        String text = data[row][col].substring(spriteMatcher.start(), spriteMatcher.end());
                        int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                        spriteCount += amount;
                    }
                    if (fantaMatcher.find()) {
                        String text = data[row][col].substring(fantaMatcher.start(), fantaMatcher.end());
                        int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                        fantaCount += amount;
                    }
                    if (stuffMatcher.find()) {
                        String text = data[row][col].substring(stuffMatcher.start(), stuffMatcher.end());
                        int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                        stuffCount += amount;
                    }
                }
            }
        }
        hookahCountLabel.setText("<html><b>" + Integer.toString(hookahCount) + "шт.</b> на сумму " + Integer.toString(hookahCount * 600) + "p.");
        teaCountLabel.setText("<html><b>" + Integer.toString(teaCount) + "шт.</b> на сумму " + Integer.toString(teaCount * 100) + "p.");
        colaCountLabel.setText("<html><b>" + Integer.toString(colaCount) + "шт.</b> на сумму " + Integer.toString(colaCount * 60) + "p.");
        spriteCountLabel.setText("<html><b>" + Integer.toString(spriteCount) + "шт.</b> на сумму " + Integer.toString(spriteCount * 60) + "p.");
        fantaCountLabel.setText("<html><b>" + Integer.toString(fantaCount) + "шт.</b> на сумму " + Integer.toString(fantaCount * 60) + "p.");
        stuffCountLabel.setText("<html><b>" + Integer.toString(stuffCount) + "шт.</b> на сумму " + Integer.toString(stuffCount * 1500) + "p.");
        String sumString = Integer.toString(hookahCount * 600 + teaCount * 100 + (colaCount + fantaCount + spriteCount) * 60 + stuffCount * 1500);
        sumLabel.setText("<html><b> Сумма за день: " + sumString + "р.</b>");

    }
}


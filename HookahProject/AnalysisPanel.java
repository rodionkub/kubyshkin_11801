package HookahProject;

import com.sun.tools.javac.comp.Flow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AnalysisPanel extends JPanel {
    JComboBox startDayComboBox = new JComboBox(MainWindow.days);
    JComboBox startMonthComboBox = new JComboBox(MainWindow.months);
    JComboBox startYearComboBox = new JComboBox(MainWindow.years);
    JComboBox endDayComboBox = new JComboBox(MainWindow.days);
    JComboBox endMonthComboBox = new JComboBox(MainWindow.months);
    JComboBox endYearComboBox = new JComboBox(MainWindow.years);
    JLabel defis = new JLabel("-");
    JButton okButton = new JButton("Выбрать промежуток");
    String[] names = {
            "Кальян",
            "Чай",
            "Кола",
            "Спрайт",
            "Фанта",
            "Стафчик"
    };
    int length = names.length;
    JPanel flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel flow2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel flow3 = new JPanel();
    JLabel totalSumTextLabel = new JLabel("Общая сумма за промежуток: ");
    JLabel totalSumLabel = new JLabel("0");
    String[][] data = new String[length][3];
    String[] columnNames = {
            "Наименование",
            "Общее количество",
            "Сумма за весь период"
    };
    JTable analysisTable;
    JScrollPane scrollPane;
    MyTableModel model;
    String[][] tempData = new String[8][14];


    public AnalysisPanel() {
        setLayout(new FlowLayout(FlowLayout.CENTER));

        setComboBoxes();
        setTable();

        flow.add(startDayComboBox);
        flow.add(startMonthComboBox);
        flow.add(startYearComboBox);
        flow.add(defis);
        flow.add(endDayComboBox);
        flow.add(endMonthComboBox);
        flow.add(endYearComboBox);
        flow.add(okButton);
        flow2.add(scrollPane);
        flow3.add(totalSumTextLabel);
        flow3.add(totalSumLabel);

        this.add(flow, BorderLayout.NORTH);
        this.add(flow2, BorderLayout.SOUTH);
        this.add(flow3, BorderLayout.EAST);
    }

    private void setComboBoxes() {
        startDayComboBox.setSelectedIndex(MainWindow.dayOfMonth);
        startMonthComboBox.setSelectedIndex(MainWindow.monthInt);
        startYearComboBox.setSelectedItem(Integer.toString(MainWindow.year));
        endDayComboBox.setSelectedIndex(MainWindow.dayOfMonth);
        endMonthComboBox.setSelectedIndex(MainWindow.monthInt);
        endYearComboBox.setSelectedItem(Integer.toString(MainWindow.year));
    }

    public void setTable() {
        for (int i = 0; i < data.length; i++) {
            data[i][0] = names[i];
        }
        countEverything();
        model = new MyTableModel(data, columnNames);
        analysisTable = new JTable(model);
        analysisTable.setGridColor(Color.BLACK);
        analysisTable.setRowHeight(25);
        scrollPane = new JScrollPane(analysisTable);


        int height = analysisTable.getRowHeight() * (analysisTable.getRowCount() + 1);
        analysisTable.setPreferredSize(new Dimension(1200, height));
        scrollPane.setPreferredSize(new Dimension(1200, height));

    }

    public void countEverything() {
        okButton.setSize(new Dimension(30, 30));
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int hookahCount = 0;
                int teaCount = 0;
                int colaCount = 0;
                int spriteCount = 0;
                int fantaCount = 0;
                int stuffCount = 0;
                int startDay = Integer.parseInt(startDayComboBox.getSelectedItem().toString());
                int startMonth = startMonthComboBox.getSelectedIndex();
                int startYear = Integer.parseInt(startYearComboBox.getSelectedItem().toString());
                int endDay = Integer.parseInt(endDayComboBox.getSelectedItem().toString());
                int endMonth = endMonthComboBox.getSelectedIndex();
                int endYear = Integer.parseInt(endYearComboBox.getSelectedItem().toString());
                if (endDay == 31) {
                    endDay = 1;
                    endMonth++;
                    if (endMonth == 13) {
                        endMonth = 1;
                        endYear++;
                    }
                } else {
                    endDay++;
                }

                while (startDay != endDay || startMonth != endMonth || startYear != endYear) {
                    Scanner sc = null;
                    try {
                        sc = new Scanner(new File("data"));
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    Pattern hookahPattern = Pattern.compile("Кальян x[1-9]");
                    Matcher hookahMatcher;
                    Pattern teaPattern = Pattern.compile("Чай x[1-9]");
                    Matcher teaMatcher;
                    Pattern colaPattern = Pattern.compile("Кола x[1-9]");
                    Matcher colaMatcher;
                    Pattern spritePattern = Pattern.compile("Спрайт x[1-9]");
                    Matcher spriteMatcher;
                    Pattern fantaPattern = Pattern.compile("Фанта x[1-9]");
                    Matcher fantaMatcher;
                    Pattern stuffPattern = Pattern.compile("Стафчик x[1-9]");
                    Matcher stuffMatcher;

                    boolean dateFound = false;

                    while (sc.hasNextLine()) {
                        String[] line = sc.nextLine().split("\\.");
                        int dataDay = Integer.parseInt(line[0]);
                        int dataMonth = Integer.parseInt(line[1]);
                        int dataYear = Integer.parseInt(line[2]);

                        if (dataDay == startDay && dataMonth == startMonth && dataYear == startYear) {
                            dateFound = true;
                            String[] dataRow;
                            String[] dataCell;
                            dataRow = line[3].split(";");
                            for (int i = 0; i < tempData.length; i++) {
                                for (int j = 1; j < tempData[0].length; j++) {
                                    dataCell = dataRow[i].split(",");
                                    if (!dataCell[j - 1].equals("null") && !dataCell[j - 1].equals(" ")) {
                                        tempData[i][j] = dataCell[j - 1];
                                    } else {
                                        tempData[i][j] = " ";
                                    }
                                }
                            }
                        }
                        if (!dateFound) {
                            for (int i = 0; i < tempData.length; i++) {
                                for (int j = 1; j < tempData[0].length; j++) {
                                    tempData[i][j] = " ";
                                }
                            }
                        }
                    }

                    for (int row = 0; row < tempData.length; row++) {
                        for (int col = 0; col < tempData[0].length; col++) {
                            if (tempData[row][col] != null) {
                                hookahMatcher = hookahPattern.matcher(tempData[row][col]);
                                teaMatcher = teaPattern.matcher(tempData[row][col]);
                                colaMatcher = colaPattern.matcher(tempData[row][col]);
                                spriteMatcher = spritePattern.matcher(tempData[row][col]);
                                fantaMatcher = fantaPattern.matcher(tempData[row][col]);
                                stuffMatcher = stuffPattern.matcher(tempData[row][col]);


                                if (hookahMatcher.find()) {
                                    String text = tempData[row][col].substring(hookahMatcher.start(), hookahMatcher.end());
                                    int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                                    hookahCount += amount;
                                }
                                if (teaMatcher.find()) {
                                    String text = tempData[row][col].substring(teaMatcher.start(), teaMatcher.end());
                                    int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                                    teaCount += amount;
                                }
                                if (colaMatcher.find()) {
                                    String text = tempData[row][col].substring(colaMatcher.start(), colaMatcher.end());
                                    int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                                    colaCount += amount;
                                }
                                if (spriteMatcher.find()) {
                                    String text = tempData[row][col].substring(spriteMatcher.start(), spriteMatcher.end());
                                    int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                                    spriteCount += amount;
                                }
                                if (fantaMatcher.find()) {
                                    String text = tempData[row][col].substring(fantaMatcher.start(), fantaMatcher.end());
                                    int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                                    fantaCount += amount;
                                }
                                if (stuffMatcher.find()) {
                                    String text = tempData[row][col].substring(stuffMatcher.start(), stuffMatcher.end());
                                    int amount = Character.getNumericValue(text.split(" ")[1].charAt(1));
                                    stuffCount += amount;
                                }
                            }
                        }
                    }

                    if (startDay == 31) {
                        startDay = 1;
                        startMonth++;
                        if (startMonth == 13) {
                            startMonth = 1;
                            startYear++;
                        }
                    } else {
                        startDay++;
                    }

                    sc.close();
                }

                data[0][1] = Integer.toString(hookahCount) + "шт.";
                analysisTable.setValueAt(hookahCount + "шт.", 0, 1);
                data[1][1] = Integer.toString(teaCount) + "шт.";
                analysisTable.setValueAt(teaCount + "шт.", 1, 1);
                data[2][1] = Integer.toString(colaCount) + "шт.";
                analysisTable.setValueAt(colaCount + "шт.", 2, 1);
                data[3][1] = Integer.toString(spriteCount) + "шт.";
                analysisTable.setValueAt(spriteCount + "шт.", 3, 1);
                data[4][1] = Integer.toString(fantaCount) + "шт.";
                analysisTable.setValueAt(fantaCount + "шт.", 4, 1);
                data[5][1] = Integer.toString(stuffCount) + "шт.";
                analysisTable.setValueAt(stuffCount + "шт.", 5, 1);

                data[0][2] = Integer.toString(hookahCount * 600) + "р.";
                analysisTable.setValueAt(hookahCount * 600 + "р.", 0, 2);
                data[1][2] = Integer.toString(teaCount * 100) + "р.";
                analysisTable.setValueAt(teaCount * 100 + "р.", 1, 2);
                data[2][2] = Integer.toString(colaCount * 60) + "р.";
                analysisTable.setValueAt(colaCount * 60 + "р.", 2, 2);
                data[3][2] = Integer.toString(spriteCount * 60) + "р.";
                analysisTable.setValueAt(spriteCount * 60 + "р.", 3, 2);
                data[4][2] = Integer.toString(fantaCount * 60) + "р.";
                analysisTable.setValueAt(fantaCount * 60 + "р.", 4, 2);
                data[5][2] = Integer.toString(stuffCount * 1500) + "р.";
                analysisTable.setValueAt(stuffCount * 1500 + "р.", 5, 2);

                int totalSum =
                        hookahCount * 600
                                + teaCount * 100
                                + colaCount * 60
                                + spriteCount * 60
                                + fantaCount * 60
                                + stuffCount * 1500;

                totalSumLabel.setText(Integer.toString(totalSum) + "р.");
            }
        });
    }


}

package HookahProject;

import com.sun.tools.corba.se.idl.constExpr.BooleanOr;
import com.sun.tools.javac.comp.Flow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckWindow extends JFrame {

    File dataFile = new File("data");
    JButton firstPlusButton = new JButton("+");
    JButton secondPlusButton = new JButton("+");
    JButton thirdPlusButton = new JButton("+");
    JButton firstMinusButton = new JButton("-");
    JButton secondMinusButton = new JButton("-");
    JButton thirdMinusButton = new JButton("-");
    JButton fourthPlusButton = new JButton("+");
    JButton fourthMinusButton = new JButton("-");
    JButton fifthPlusButton = new JButton("+");
    JButton fifthMinusButton = new JButton("-");
    JButton sixthPlusButton = new JButton("+");
    JButton sixthMinusButton = new JButton("-");
    String[] items = {
            " ",
            "Кальян",
            "Чай",
            "Кола",
            "Спрайт",
            "Фанта",
            "Стафчик"
    };
    JComboBox<String> item1 = new JComboBox<>(items);
    JComboBox<String> item2 = new JComboBox<>(items);
    JComboBox<String> item3 = new JComboBox<>(items);
    JComboBox<String> item4 = new JComboBox<>(items);
    JComboBox<String> item5 = new JComboBox<>(items);
    JComboBox<String> item6 = new JComboBox<>(items);
    JLabel firstCount = new JLabel("0");
    JLabel secondCount = new JLabel("0");
    JLabel thirdCount = new JLabel("0");
    JLabel fourthCount = new JLabel("0");
    JLabel fifthCount = new JLabel("0");
    JLabel sixthCount = new JLabel("0");
    JButton button = new JButton("Добавить");
    JPanel panel = new JPanel();

    public CheckWindow(String text, int row, int column) {
        super("What did he get, huh?");
        WindowListener exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
            }
        };
        addWindowListener(exitListener);

        setLayout(new BorderLayout());
        panel.setLayout(new GridLayout(3, 1));

        fillComboBoxes(text);
        setAddButton(row, column);
        setPlusMinusButtons();
        addAllTheShit();

        getContentPane().add(panel, BorderLayout.NORTH);
        getContentPane().add(button, BorderLayout.SOUTH);

        setSize(400, 200);
        setMaximumSize(new Dimension(400,200));
        setMinimumSize(new Dimension(400,200));
    }

    public void fillComboBoxes(String text) {
        Pattern hookahPattern = Pattern.compile("Кальян x[1-9]");
        Matcher hookahMatcher = hookahPattern.matcher(text);
        Pattern teaPattern = Pattern.compile("Чай x[1-9]");
        Matcher teaMatcher = teaPattern.matcher(text);
        Pattern colaPattern = Pattern.compile("Кола х[1-9]");
        Matcher colaMatcher = colaPattern.matcher(text);
        Pattern spritePattern = Pattern.compile("Спрайт х[1-9]");
        Matcher spriteMatcher = spritePattern.matcher(text);
        Pattern fantaPattern = Pattern.compile("Фанта х[1-9]");
        Matcher fantaMatcher = fantaPattern.matcher(text);
        Pattern stuffPattern = Pattern.compile("Стафчик х[1-9]");
        Matcher stuffMatcher = stuffPattern.matcher(text);
        Pattern numberPattern = Pattern.compile("[1-9]");
        Matcher numberMatcher;

        if (hookahMatcher.find()) {
            item1.setSelectedIndex(1);
            String line = text.substring(hookahMatcher.start(), hookahMatcher.end());
            numberMatcher = numberPattern.matcher(line);
            if (numberMatcher.find()) {
                String numberLine = line.substring(numberMatcher.start(), numberMatcher.end());
                firstCount.setText(numberLine);
            }
        }
        if (teaMatcher.find()) {
            item2.setSelectedIndex(2);
            String line = text.substring(teaMatcher.start(), teaMatcher.end());
            numberMatcher = numberPattern.matcher(line);
            if (numberMatcher.find()) {
                String numberLine = line.substring(numberMatcher.start(), numberMatcher.end());
                secondCount.setText(numberLine);
            }
        }
        if (colaMatcher.find()) {
            item3.setSelectedIndex(3);
            String line = text.substring(colaMatcher.start(), colaMatcher.end());
            numberMatcher = numberPattern.matcher(line);
            if (numberMatcher.find()) {
                String numberLine = line.substring(numberMatcher.start(), numberMatcher.end());
                secondCount.setText(numberLine);
            }
        }
        if (spriteMatcher.find()) {
            item4.setSelectedIndex(4);
            String line = text.substring(spriteMatcher.start(), spriteMatcher.end());
            numberMatcher = numberPattern.matcher(line);
            if (numberMatcher.find()) {
                String numberLine = line.substring(numberMatcher.start(), numberMatcher.end());
                secondCount.setText(numberLine);
            }
        }
        if (fantaMatcher.find()) {
            item5.setSelectedIndex(5);
            String line = text.substring(fantaMatcher.start(), fantaMatcher.end());
            numberMatcher = numberPattern.matcher(line);
            if (numberMatcher.find()) {
                String numberLine = line.substring(numberMatcher.start(), numberMatcher.end());
                secondCount.setText(numberLine);
            }
        }
        if (stuffMatcher.find()) {
            item2.setSelectedIndex(6);
            String line = text.substring(stuffMatcher.start(), stuffMatcher.end());
            numberMatcher = numberPattern.matcher(line);
            if (numberMatcher.find()) {
                String numberLine = line.substring(numberMatcher.start(), numberMatcher.end());
                secondCount.setText(numberLine);
            }
        }
    }

    public void addAllTheShit() {
        JPanel flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel flow2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel flow3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel flow4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel flow5 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel flow6 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        flow.add(item1);
        flow.add(firstPlusButton);
        flow.add(firstCount);
        flow.add(firstMinusButton);
        flow2.add(item2);
        flow2.add(secondPlusButton);
        flow2.add(secondCount);
        flow2.add(secondMinusButton);
        flow3.add(item3);
        flow3.add(thirdPlusButton);
        flow3.add(thirdCount);
        flow3.add(thirdMinusButton);
        flow4.add(item4);
        flow4.add(fourthPlusButton);
        flow4.add(fourthCount);
        flow4.add(fourthMinusButton);
        flow5.add(item5);
        flow5.add(fifthPlusButton);
        flow5.add(fifthCount);
        flow5.add(fifthMinusButton);
        flow6.add(item6);
        flow6.add(sixthPlusButton);
        flow6.add(sixthCount);
        flow6.add(sixthMinusButton);

        panel.add(flow);
        panel.add(flow2);
        panel.add(flow3);
        panel.add(flow4);
        panel.add(flow5);
        panel.add(flow6);
    }

    public void setAddButton(int row, int column) {
        button.setSize(new Dimension(70, 60));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = "";
                if (!firstCount.getText().equals("0")) {
                    text += "<html>" + item1.getSelectedItem() + " x" + firstCount.getText() + "<br>";
                }
                if (!secondCount.getText().equals("0")) {
                    text += "<html>" + item2.getSelectedItem() + " x" + secondCount.getText() + "<br>";
                }
                if (!thirdCount.getText().equals("0")) {
                    text += "<html>" + item3.getSelectedItem() + " x" + thirdCount.getText() + "<br>";
                }
                if (!fourthCount.getText().equals("0")) {
                    text += "<html>" + item4.getSelectedItem() + " x" + fourthCount.getText() + "<br>";
                }
                if (!fifthCount.getText().equals("0")) {
                    text += "<html>" + item5.getSelectedItem() + " x" + fifthCount.getText() + "<br>";
                }
                if (!sixthCount.getText().equals("0")) {
                    text += "<html>" + item6.getSelectedItem() + " x" + sixthCount.getText() + "<br>";
                }

                MainWindow.table.setValueAt(text, row, column);
                MainWindow.data[row][column] = text;

                FileWriter fr = null;
                String str = MainWindow.dayOfMonth + "." + MainWindow.monthInt + "." + MainWindow.year;
                String dataStr = "";
                for (int i = 0; i < MainWindow.data.length; i++) {
                    for (int j = 1; j < MainWindow.data[0].length; j++) {
                        dataStr += MainWindow.data[i][j];
                        if (j != MainWindow.data[0].length - 1) {
                            dataStr += ",";
                        }
                    }
                    dataStr += ";";
                }
                int length = dataStr.length();
                dataStr = dataStr.substring(0, length - 1);
                try {
                    fr = new FileWriter(dataFile, true);
                    fr.write(str + "." + dataStr + "\n");
                } catch (IOException e1) {
                    e1.printStackTrace();
                } finally {
                    try {
                        assert fr != null;
                        fr.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                MainWindow.sumButton.doClick();
                CheckWindow.this.setVisible(false);
            }
        });
    }

    public void setPlusMinusButtons() {
        firstPlusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item1.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(firstCount.getText());
                    countInt++;
                    firstCount.setText(Integer.toString(countInt));
                }
            }
        });
        secondPlusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item2.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(secondCount.getText());
                    countInt++;
                    secondCount.setText(Integer.toString(countInt));
                }
            }
        });
        thirdPlusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item3.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(thirdCount.getText());
                    countInt++;
                    thirdCount.setText(Integer.toString(countInt));
                }
            }
        });
        firstMinusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item1.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(firstCount.getText());
                    if (countInt != 0) {
                        countInt--;
                    }
                    firstCount.setText(Integer.toString(countInt));
                }
            }
        });
        secondMinusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item2.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(secondCount.getText());
                    if (countInt != 0) {
                        countInt--;
                    }
                    secondCount.setText(Integer.toString(countInt));
                }
            }
        });
        thirdMinusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item3.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(thirdCount.getText());
                    if (countInt != 0) {
                        countInt--;
                    }
                    thirdCount.setText(Integer.toString(countInt));
                }
            }
        });
        fourthPlusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item4.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(fourthCount.getText());
                    countInt++;
                    fourthCount.setText(Integer.toString(countInt));
                }
            }
        });
        fourthMinusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item4.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(fourthCount.getText());
                    if (countInt != 0) {
                        countInt--;
                    }
                    fourthCount.setText(Integer.toString(countInt));
                }
            }
        });
        fifthPlusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item5.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(fifthCount.getText());
                    countInt++;
                    fifthCount.setText(Integer.toString(countInt));
                }
            }
        });
        fifthMinusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item5.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(fifthCount.getText());
                    if (countInt != 0) {
                        countInt--;
                    }
                    fifthCount.setText(Integer.toString(countInt));
                }
            }
        });
        sixthPlusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item6.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(sixthCount.getText());
                    countInt++;
                    sixthCount.setText(Integer.toString(countInt));
                }
            }
        });
        sixthMinusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!item6.getSelectedItem().equals(" ")) {
                    int countInt = Integer.parseInt(sixthCount.getText());
                    if (countInt != 0) {
                        countInt--;
                    }
                    sixthCount.setText(Integer.toString(countInt));
                }
            }
        });

        firstPlusButton.setPreferredSize(new Dimension(20, 20));
        secondPlusButton.setPreferredSize(new Dimension(20, 20));
        thirdPlusButton.setPreferredSize(new Dimension(20, 20));
        firstMinusButton.setPreferredSize(new Dimension(20, 20));
        secondMinusButton.setPreferredSize(new Dimension(20, 20));
        thirdMinusButton.setPreferredSize(new Dimension(20, 20));
        fourthPlusButton.setPreferredSize(new Dimension(20, 20));
        fifthPlusButton.setPreferredSize(new Dimension(20, 20));
        sixthPlusButton.setPreferredSize(new Dimension(20, 20));
        fourthMinusButton.setPreferredSize(new Dimension(20, 20));
        fifthMinusButton.setPreferredSize(new Dimension(20, 20));
        sixthMinusButton.setPreferredSize(new Dimension(20, 20));
    }
}

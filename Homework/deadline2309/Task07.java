/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 07
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double x = in.nextDouble();

        final double EPS = 1e-9;

        double currentElX = 1;
        double prevElX = 1;
        double powX = 1;
        int facX = 1;
        int k = 1;
        double sum = 1;

        while ((currentElX > 0 ? currentElX : -currentElX) > EPS) {
            powX *= x;
            facX *= k;
            currentElX = powX / facX;

            if (k % 4 == 2) {
                sum = prevElX - currentElX;
                prevElX = sum;

            }
            else if (k % 4 == 0) {
                sum = prevElX + currentElX;
                prevElX = sum;
            }
            k++;
        }

        System.out.println(sum);
    }
}

/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 06
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double x = in.nextDouble();

        final double EPS = 1e-9;

        double currentElX = 1;
        double prevElX = x - 1;
        double powX = x - 1;
        int k = 2;
        double sum = 0;

        while ((currentElX > 0 ? currentElX : -currentElX) > EPS) {
            powX *= (x - 1);
            currentElX = powX / k;

            if (k % 2 == 0) {
                sum = prevElX - currentElX;
                prevElX = sum;

            }
            else if (k % 2 == 1) {
                sum = prevElX + currentElX;
                prevElX = sum;
            }
            k++;
        }

        System.out.println(sum);
    }
}

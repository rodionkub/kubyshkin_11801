/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 09
 */

package Homework.deadline2309;

public class Task09 {
    public static void main(String[] args) {
        int[] a = {132, 13576, 35713};

        System.out.println(isCorrectLength(a));
    }

    public static boolean isCorrectLength(int[] a) {
        int countDigits = 0;
        int countNumbers = 0;
        for (int x : a) {
            int checkX = x;
            while (x != 0) {
                countDigits++;
                x /= 10;
            }
            if ((countDigits == 3 || countDigits == 5) && (areAllDigitsEven(checkX) || areAllDigitsOdd(checkX))) {
                countNumbers++;
            }
            if (countNumbers > 2) {
                return false;
            }
            countDigits = 0;
        }
        if (countNumbers == 2) {
            return true;
        }
        else {
            return false;
        }

    }

    public static boolean areAllDigitsEven (int x) {
        while (x != 0) {
            if (x % 2 != 0) {
                return false;
            }
            x /= 10;
        }
        return true;

    }

    public static boolean areAllDigitsOdd (int x) {
        while (x != 0) {
            if (x % 2 == 0) {
                return false;
            }
            x /= 10;
        }
        return true;

    }


}

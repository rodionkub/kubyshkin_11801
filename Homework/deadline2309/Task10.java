package Homework.deadline2309;

public class Task10 {
    public static void main(String[] args) {
        int[] a = {321, 125, 7541, 8321, 9531};

        System.out.println(inDescendingOrder(a));
    }

    public static boolean inDescendingOrder(int[] a) {
        int countCorrectDigit = 0;
        int countDigits = 0;
        int countNumbers = 0;
        int previousDigit;
        for (int x : a) {
            while (x != 0) {
                previousDigit = x % 10;
                x /= 10;
                if (x % 10 > previousDigit) {
                    countCorrectDigit++;
                }
                countDigits++;
            }
            if (countCorrectDigit == (countDigits - 1)) {
                countNumbers++;
            }
            if (countNumbers > 3) {
                return false;
            }
            countCorrectDigit = 0;
            countDigits = 0;
        }
        if (countNumbers == 3) {
            return true;
        }
        else {
            return false;
        }

    }
}
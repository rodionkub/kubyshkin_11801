package Homework.deadline2309;

public class Task12 {
    public static void main(String[] args) {
        int[] a = {3, 2, 4, 2, 1};
        boolean result = false;

        if (a[0] > a[1]) {
            result = true;
        }
        if (a[a.length - 1] > a[a.length - 2]) {
            result = true;
        }
        for (int i = 1; i < a.length - 2; i++) {
            if (a[i] > a[i - 1] && a[i] > a[i + 1] && a[i] % 2 == 0) {
                result = true;
            }
        }

        System.out.println(result);
    }


}

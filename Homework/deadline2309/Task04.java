/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 04
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int r = in.nextInt();
        double backsideR = 1.0 / r;
        int n;

        for (int i = r; i >= -r; i--) {
            n = (int) (Math.cos(Math.asin(backsideR * Math.abs(i))) * r);
            for (int j = 0; j <= r - n; j++) {
                System.out.print("*");
            }
            for (int j = 0; j <= 2 * n; j++) {
                System.out.print("0");
            }
            for (int j = 0; j <= r - n; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}

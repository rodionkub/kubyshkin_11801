package Homework.deadline2309.Abstraction;

public class Teacher extends Person {
    private String subject;


    public Teacher(String name, int age) {
        super(name, age);
    }

    public Teacher(String name, int age, String subject) {
        this(name, age);
        this.subject = subject;
    }

    public void threat() {
        System.out.println("You want to stay here for another hour!?");
    }

    public void setSubject(String subject) { this.subject = subject; }

    public String getSubject() { return subject; }

    public void sayHello() {
        System.out.println(this.getName() + " says: " + "Hello, student!");
    }

    @Override
    public void sayFavoritePhrase() {
        System.out.println(this.getName() + " says: " + "You haven't passed the exam :(");
    }
}

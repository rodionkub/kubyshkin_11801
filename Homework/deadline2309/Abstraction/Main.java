package Homework.deadline2309.Abstraction;

public class Main {
    public static void main(String[] args) {
        Person redbeard = new Teacher("Mr. Redbeard", 30);
        Person holmes = new Teacher("Mr. Holmes", 31, "Science of Deduction");
        Person moriarty = new Teacher("Mr. Moriarty", 28, "Crime");

        Person[] upcastingArray = {
                redbeard,
                holmes,
                moriarty
        };

        int length = upcastingArray.length;
        for (int i = 0; i < length; i++) {
            upcastingArray[i].sayHello();
            upcastingArray[i].sayFavoritePhrase();
            System.out.println();
        }
    }

}

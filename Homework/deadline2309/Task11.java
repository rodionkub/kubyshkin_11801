package Homework.deadline2309;

public class Task11 {
    public static void main(String[] args) {
        int k = 4;
        int n = 2312;
        int result = 0;

        int degree = 1;

        int length = howManyDigits(n);

        for (int i = 0; i < length; i++) {
            result += (n % 10) * degree;
            degree *= k;
            n /= 10;
        }

        System.out.println(result);
    }

    public static int howManyDigits(int n) {
        int count = 0;
        while (n != 0) {
            count++;
            n /= 10;
        }
        return count;
    }

}

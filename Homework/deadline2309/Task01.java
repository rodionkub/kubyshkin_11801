/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 01
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
    Scanner in = new Scanner(System.in);

    int n = in.nextInt();
    int initialN = n;

    int k = 2 * n + 2;
    int count = 1;
    while (n != 0) {
        for (int i = 0; i < 2 * n + count; i++) {
            System.out.print(" ");
        }
        for (int i = 2 * n + 1; i < k; i++) {
            System.out.print("*");
        }
        System.out.println();
        n--;
        count++;
    }
    System.out.println();

    n = initialN;
    k = n + 1;
    count = 1;

    while (n != 0) {
        for (int i = 0; i < n; i++) {
            System.out.print(" ");
        }
        for (int i = n; i < k; i++) {
            System.out.print("*");
        }
        for (int i = n; i < 3*n + 1; i++) {
            System.out.print(" ");
        }
        for (int i = 2*n + 1; i < 2*n + 1 + count; i++) {
            System.out.print("*");
        }
        System.out.println();
        n--;
        k++;
        count += 2;
    }

    }
}

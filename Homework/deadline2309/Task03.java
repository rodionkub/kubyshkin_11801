/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 03
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double x = in.nextDouble();

        final double EPS = 1e-6;

        double maxSqrt = 1;
        double mid = 2;

        while (maxSqrt * maxSqrt < x) {
            maxSqrt++;
        }
        double minSqrt = maxSqrt - 1;
        while ((maxSqrt - minSqrt > 0 ? maxSqrt - minSqrt : minSqrt - maxSqrt)  > EPS) {
            mid = Math.abs(maxSqrt + minSqrt) / 2;
            if (mid * mid > x) {
                maxSqrt = mid;
            }
            else if (mid * mid < x) {
                minSqrt = mid;
            }
            else {
                System.out.println(mid);
                break;
            }
        }
        System.out.printf("%.6f", mid);

    }
}

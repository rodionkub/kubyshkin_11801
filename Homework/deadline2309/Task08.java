/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 08
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double x = in.nextDouble();

        final double EPS = 1e-9;

        double currentElX = x;
        double prevElX = x;
        double powX = 1;
        int facX = 1;
        int k = 1;
        double sum = 0;

        while ((currentElX > 0 ? currentElX : -currentElX) > EPS) {
            powX *= x;
            facX *= k;
            currentElX = powX / facX;

            if (k % 4 == 3) {
                sum = prevElX - currentElX;
                prevElX = sum;

            }
            else if (k % 4 == 1 && k != 1) {
                sum = prevElX + currentElX;
                prevElX = sum;
            }
            k++;
        }

        System.out.println(sum);
    }
}

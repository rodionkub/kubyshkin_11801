package Homework.deadline2309.OOPTasks.Task01;

public class Player {
    private String name;
    private int hp;

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    public int getHP() { return hp; }

    public String getName() { return name; }

    public void isHitFor(int strength) { this.hp -= strength; }


}

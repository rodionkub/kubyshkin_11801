package Homework.deadline2309.OOPTasks.Task01;

import java.util.Scanner;

public class Game {
    private int strength;
    private Player player1;
    private Player player2;
    public Game (Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public void setStrength(int strength) { this.strength = strength; }

    public int getStrength() { return strength; }

    public void startGame() {
        int i = 1;
        startGameText();
        while (someoneIsAlive()) {
            yourTurn(i);
            hitStrength(i);
            setStrength(input());
            dealDamage(i);
            showHP(i);
            i++;
        }
        gameResult();
    }

    public int input() {
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    public boolean someoneIsAlive() {
        return player1.getHP() > 0 && player2.getHP() > 0;
    }

    public void startGameText() {
        System.out.println("Да начнется битва!");
    }

    public void hitStrength(int i) {
        if (i % 2 == 1) {
            System.out.println("Бьет " + player1.getName() + "!");
        }
        else {
            System.out.println("Бьет " + player2.getName() + "!");
        }
        System.out.print("Сила удара: ");
    }

    public String yourTurn(int i) {
        if (i % 2 == 1) {
            return player1.getName() + ", твой ход!";
        }
        else {
            return player2.getName() + ", твой ход!";
        }
    }

    public void dealDamage(int i) {
        if (i % 2 == 1) {
            player2.isHitFor(getStrength());
        }
        else {
            player1.isHitFor(getStrength());
        }
    }

    public void showHP(int i) {
        System.out.print("Ауч! ");
        if (i % 2 == 1) {
            System.out.println(player2.getName() + " получил лицу на " +
                    getStrength() + "! У него осталось " + player2.getHP() + " здоровья.");
        }
        else {
            System.out.println(player1.getName() + " получил лицу на " +
                    getStrength() + "! У него осталось " + player1.getHP() + " здоровья.");

        }
        System.out.println();
    }

    public void gameResult() {
        if (player1.getHP() <= 0) {
            System.out.println("Победа игрока " + player2.getName() + "! Поздравляем!");
        }
        else if (player2.getHP() <= 0) {
            System.out.println("Победа игрока " + player1.getName() + "! Поздравляем!");
        }
    }
}

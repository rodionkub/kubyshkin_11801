package Homework.deadline2309.LastFromEachBlock;

public class Task16 {
    public static void main(String[] args) {
        int[] a = {0, 1, 2, 3, 4};
        int[] b = {4, 1, 3, 0, 2};
        mul(a, b);
    }
    public static void mul(int[] a, int[] b) {
        int[] result = new int[a.length];

        for (int i = 0; i < a.length; i++) {
            result[i] = a[b[i]];
            System.out.print(result[i] + " ");
        }
    }
}

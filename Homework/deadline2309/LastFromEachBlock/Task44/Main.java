package Homework.deadline2309.LastFromEachBlock.Task44;

public class Main {
    public static void main(String[] args) {
        Player player1 = new Player("Rodion", 10);
        Player player2 = new Player("Emil", 10);
        Game game = new Game(player1, player2);
        game.startGame();
    }
}

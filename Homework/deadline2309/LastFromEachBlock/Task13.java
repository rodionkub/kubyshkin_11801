package Homework.deadline2309.LastFromEachBlock;

public class Task13 {
    public static void main(String[] args) {
        int[] a = {4, 2, 4, 2, 1};
        int count = 0;

        if (a[0] > a[1] && a[0] % 2 == 0) {
            count++;
        }
        if (a[a.length - 1] > a[a.length - 2] && a[a.length - 1] % 2 == 0) {
            count++;
        }
        for (int i = 1; i < a.length - 2; i++) {
            if (a[i] > a[i - 1] && a[i] > a[i + 1] && a[i] % 2 == 0) {
                count++;
            }
        }
        if (count == 2) {
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }
    }


}

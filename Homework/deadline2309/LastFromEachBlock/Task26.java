package Homework.deadline2309.LastFromEachBlock;

public class Task26 {
    public static void main(String[] args) {
        int a[][][] = {
                {
                        {1, 2, 3},
                        {3, 6, 9}
                },
                {
                        {3, 6, 9},
                        {3, 6, 3}
                },
                {
                        {3, 6, 9}
                }
        };

        doesItFitMyQuestion(a);
    }

    public static void doesItFitMyQuestion(int[][][] a) {
        int countNumbersInOneString = 0;
        boolean flag = false;
        int countCorrectArrays = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length && !flag; j++) {
                for (int k = 0; k < a[i][j].length; k++) {
                    if (a[i][j][k] % 3 == 0) {
                        countNumbersInOneString++;
                    }
                    if (countNumbersInOneString == a[i][j].length) {
                        flag = true;
                    }
                }
                countNumbersInOneString = 0;
                if (flag) {
                    countCorrectArrays++;
                }
            }
            flag = false;
        }

        if (countCorrectArrays == a.length) {
            System.out.println("Oh yeah!");
        }
        else {
            System.out.println("No");
        }
    }
}

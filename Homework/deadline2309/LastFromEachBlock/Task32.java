package Homework.deadline2309.LastFromEachBlock;

public class Task32 {
    public static void main(String[] args) {
        String englishText = "mom haha lol dad mom loves kittens";
        StringBuilder patriarchicEnglishText = new StringBuilder();

        int length = englishText.length();
        for (int i = 0; i < length; i++) {
            if (englishText.charAt(i) == 'm' && englishText.charAt(i + 1) == 'o' && englishText.charAt(i + 2) == 'm') {
                patriarchicEnglishText.append("dad");
                i += 2;
            }
            else {
                patriarchicEnglishText.append("" + englishText.charAt(i));
            }
        }
        englishText = patriarchicEnglishText.toString();

        System.out.println(englishText);
    }
}

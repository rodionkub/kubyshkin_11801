package Homework.deadline2309.LastFromEachBlock.Task59;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ArrayList<Browser> browsers = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        init();
        fastestSite();
        fastestBrowser();
    }

    public static double min(double a, double b) {
        return a < b ? a : b;
    }

    public static void init() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("/Users/rodionkub/IdeaProjects/lesson5.09/src/Homework/deadline2309/LastFromEachBlock/Task59/Browser_Speed.csv"));

        sc.nextLine();

        while (sc.hasNextLine()) {
            String line = sc.nextLine();

            browsers.add(new Browser(
                    line.split(",")[0],
                    Double.parseDouble(line.split(",")[1]),
                    Double.parseDouble(line.split(",")[2]),
                    Double.parseDouble(line.split(",")[3]),
                    Double.parseDouble(line.split(",")[4]),
                    Double.parseDouble(line.split(",")[5]),
                    Double.parseDouble(line.split(",")[6]),
                    Double.parseDouble(line.split(",")[7]),
                    Double.parseDouble(line.split(",")[8]),
                    Double.parseDouble(line.split(",")[9]),
                    Double.parseDouble(line.split(",")[10])
            ));
        }
    }

    public static void fastestSite() {
        double amazon = 0;
        double apple = 0;
        double ebay = 0;
        double microsoft = 0;
        double myspace = 0;
        double pcworld = 0;
        double wikipedia = 0;
        double yahoo = 0;
        double youtube = 0;

        for (int i = 0; i < browsers.size(); i++) {
            amazon += browsers.get(i).getAmazon();
            apple += browsers.get(i).getApple();
            ebay += browsers.get(i).getEbay();
            microsoft += browsers.get(i).getMicrosoft();
            myspace += browsers.get(i).getMyspace();
            pcworld += browsers.get(i).getPcworld();
            wikipedia += browsers.get(i).getWikipedia();
            yahoo += browsers.get(i).getYahoo();
            youtube += browsers.get(i).getYoutube();
        }

        amazon /= browsers.size();
        apple /= browsers.size();
        ebay /= browsers.size();
        microsoft /= browsers.size();
        myspace /= browsers.size();
        pcworld /= browsers.size();
        wikipedia /= browsers.size();
        yahoo /= browsers.size();
        youtube /= browsers.size();

        double min = min(min(min(min(min(amazon, apple), min(ebay, microsoft)), min(myspace, pcworld)), min(wikipedia, yahoo)), youtube);

        if (min == amazon) {
            System.out.println("Amazon");
        }
        else if (min == apple) {
            System.out.println("Apple");
        }
        else if (min == ebay) {
            System.out.println("eBay");
        }
        else if (min == microsoft) {
            System.out.println("Microsoft");
        }
        else if (min == pcworld) {
            System.out.println("PCWorld");
        }
        else if (min == wikipedia) {
            System.out.println("Wikipedia");
        }
        else if (min == yahoo) {
            System.out.println("Yahoo");
        }
        else if (min == youtube) {
            System.out.println("YouTube");
        }
    }
    
    public static void fastestBrowser() {
        double amazon = 0;
        double apple = 0;
        double ebay = 0;
        double microsoft = 0;
        double myspace = 0;
        double pcworld = 0;
        double wikipedia = 0;
        double yahoo = 0;
        double youtube = 0;

        double avg = 0;
        double min = 10;
        int minIndex = 0;

        for (int i = 0; i < browsers.size(); i++) {
            amazon = browsers.get(i).getAmazon();
            apple = browsers.get(i).getApple();
            ebay = browsers.get(i).getEbay();
            microsoft = browsers.get(i).getMicrosoft();
            myspace = browsers.get(i).getMyspace();
            pcworld = browsers.get(i).getPcworld();
            wikipedia = browsers.get(i).getWikipedia();
            yahoo = browsers.get(i).getYahoo();
            youtube = browsers.get(i).getYoutube();

            avg = (amazon + apple + ebay + microsoft + myspace + pcworld + wikipedia + yahoo + youtube) / 9;
            if (avg < min) {
                min = avg;
                minIndex = i;
            }
        }

        System.out.println(browsers.get(minIndex).getBrowser());
    }
}

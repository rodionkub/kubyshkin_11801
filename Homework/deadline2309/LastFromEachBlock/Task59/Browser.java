package Homework.deadline2309.LastFromEachBlock.Task59;

public class Browser {
    private String browser;
    private double average;
    private double amazon;
    private double apple;
    private double ebay;
    private double microsoft;
    private double myspace;
    private double pcworld;
    private double wikipedia;
    private double yahoo;
    private double youtube;

    public Browser(String browser, double average, double amazon, double apple, double ebay, double microsoft, double myspace, double pcworld, double wikipedia, double yahoo, double youtube) {
        this.browser = browser;
        this.average = average;
        this.amazon = amazon;
        this.apple = apple;
        this.ebay = ebay;
        this.microsoft = microsoft;
        this.myspace = myspace;
        this.pcworld = pcworld;
        this.wikipedia = wikipedia;
        this.yahoo = yahoo;
        this.youtube = youtube;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getAmazon() {
        return amazon;
    }

    public void setAmazon(double amazon) {
        this.amazon = amazon;
    }

    public double getApple() {
        return apple;
    }

    public void setApple(double apple) {
        this.apple = apple;
    }

    public double getEbay() {
        return ebay;
    }

    public void setEbay(double ebay) {
        this.ebay = ebay;
    }

    public double getMicrosoft() {
        return microsoft;
    }

    public void setMicrosoft(double microsoft) {
        this.microsoft = microsoft;
    }

    public double getMyspace() {
        return myspace;
    }

    public void setMyspace(double myspace) {
        this.myspace = myspace;
    }

    public double getPcworld() {
        return pcworld;
    }

    public void setPcworld(double pcworld) {
        this.pcworld = pcworld;
    }

    public double getWikipedia() {
        return wikipedia;
    }

    public void setWikipedia(double wikipedia) {
        this.wikipedia = wikipedia;
    }

    public double getYahoo() {
        return yahoo;
    }

    public void setYahoo(double yahoo) {
        this.yahoo = yahoo;
    }

    public double getYoutube() {
        return youtube;
    }

    public void setYoutube(double youtube) {
        this.youtube = youtube;
    }
}

package Homework.deadline2309.LastFromEachBlock;

import java.util.Scanner;

public class Task19 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String longNumber = in.nextLine();
        int shortNumber = in.nextInt();

        System.out.println(longMultiply(longNumber, shortNumber));
    }

    static String longMultiply(String a, int b) {
        StringBuilder p = new StringBuilder();
        int carry = 0;
        int length = a.length();
        for (int i = length - 1; i >= 0; i--) {
            carry += Character.getNumericValue(a.charAt(i)) * b;
            p.append(carry % 10);
            carry /= 10;
        }

        p.reverse();
        return p.toString();

    }
}


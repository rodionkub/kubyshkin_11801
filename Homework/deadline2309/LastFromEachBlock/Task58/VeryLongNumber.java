package Homework.deadline2309.LastFromEachBlock.Task58;

import Homework.deadline2309.LastFromEachBlock.Task58.Interfaces.Number;

public class VeryLongNumber implements Number {
    private String number;

    public VeryLongNumber(String number) {
        this.number = number;
    }

    public String getNumber() { return number; }

    @Override
    public Number add(Number n) {
        String str1 = findMaxNumber(this, n);
        String str2 = fillWithZeros(findMinNumber(this, n), maxNumberLength(str1));
        String result = "000";
        int l = maxNumberLength(str1);

        for (int i = l; i > 0; i--) {
            //result.charAt(i) = (char) ((str1.charAt(i) + str2.charAt(i)) % 10);
        }

        return null;
    }

    @Override
    public Number sub(Number n) {
        return null;
    }

    @Override
    public int compareTo(Number n) {
        return 0;
    }

    private String findMinNumber(Number number1, Number number2) {
        return number1.getNumber().length() <= number2.getNumber().length() ? number1.getNumber() : number2.getNumber();
    }

    private String findMaxNumber(Number number1, Number number2) {
        return number1.getNumber().length() <= number2.getNumber().length() ? number2.getNumber() : number1.getNumber();
    }

    private int maxNumberLength(String number) {
        return number.length();
    }

    private String fillWithZeros(String number, int length) {
        int minLength = number.length();
        int amountOfZeros = length - minLength;
        for (int i = 0; i < amountOfZeros; i++) {
            number = "0" + number;
        }
        return number;
    }
}

package Homework.deadline2309.LastFromEachBlock;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task38 {
    public static void main(String[] args) {
        Random random = new Random();
        Pattern pattern = Pattern.compile("(0||2||4||6||8)(0||2||4||6||8)");
        Matcher matcher;

        int[] a = new int[10];
        boolean flag = false;
        int count = 0;
        int countGeneratedNumbers = 0;
        int x;


        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(1000);
            countGeneratedNumbers++;
            matcher = pattern.matcher(Integer.toString(a[i]));
            if (matcher.matches()) {
                count++;
            }
        }

        if (count >= 2) {
            System.out.println("Done. Generated in total: " + countGeneratedNumbers + ". The array is: ");
            for (int i = 0; i < a.length; i++) {
                System.out.println(a[i]);
            }
        }
        else {
            while (!flag) {
                count = 0;
                x = random.nextInt(1000);
                countGeneratedNumbers++;

                for (int i = 0; i < a.length - 1; i++) {
                    a[i] = a[i + 1];
                }
                a[a.length - 1] = x;
                for (int i = 0; i < a.length; i++) {
                    matcher = pattern.matcher(Integer.toString(a[i]));
                    if (matcher.matches()) {
                        count++;
                    }
                }
                if (count >= 2) {
                    System.out.println("Done. Generated in total: " + countGeneratedNumbers + ". The array is: ");
                    for (int i = 0; i < a.length; i++) {
                        System.out.println(a[i]);
                    }
                    flag = true;
                }
            }
        }

    }
}

/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 05
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        double x = in.nextDouble();
        double elX = 1, sum = elX;
        int k = 1;

        final double EPS = 1e-9;


        while ((elX > 0 ? elX : -elX) > EPS) {
            elX = elX * x / k;
            sum += elX;
            k++;
        }
        System.out.println(sum);
    }
}

/**
 * @author Rodion Kubyshkin
 * 11-801
 * Task 02
 */

package Homework.deadline2309;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int k = in.nextInt();
        int m = in.nextInt();

        int min = k > m ? m : k;
        int max = k > m ? k : m;
        min += 1;
        max -= 1;

        while (min % 3 != 0) {
            min++;
        }
        while (min < max) {
            System.out.println(min);
            min += 3;
        }
    }
}

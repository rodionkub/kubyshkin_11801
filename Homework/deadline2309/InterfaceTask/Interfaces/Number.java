package Homework.deadline2309.InterfaceTask.Interfaces;

public interface Number {
    public Number add(Number n);
    public Number sub(Number n);
    public int compareTo(Number n);
    public String getNumber();
}

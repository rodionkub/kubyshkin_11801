package Classwork.lesson1503;
import java.util.Random;

public class Main {
    private static int k;

    public static void main(String[] args) {
        Tree tree = new Tree();
        k = 1;
        create(15, tree, 0);
    }

    private static Tree create(int n, Tree tree, int step) {
        if (n > 0) {
            int root = k;
            tree.addRoot(root);
            int v;
            if ((n - 1) / 2 > 0 && (n - 1) / 2 < 1) {
                v = 1;
            }
            else {
                v = (n - 1) / 2;
            }
            String s = "";
            for (int i = 0; i < step; i++) {
                s = s.concat(" ");
            }
            s = s.concat(String.valueOf(k));
            System.out.println(s);
            k++;
            step++;
            tree.addLeft(create(v, new Tree(), step));
            tree.addRight(create(n - v - 1, new Tree(), step));
        }
        // System.out.println(tree);
        return tree;
    }
}

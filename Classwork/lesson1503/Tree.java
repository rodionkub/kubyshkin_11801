package Classwork.lesson1503;

public class Tree {
    private int root = -1;
    private Tree left, right = null;

    public Tree() {
    }

    public void addRoot(int root) {
        this.root = root;
    }

    public void addLeft(Tree left) {
        this.left = left;
    }

    public void addRight(Tree right) {
        this.right = right;
    }

    public boolean hasRoot() {
        return root != -1;
    }


    @Override
    public String toString() {
        if (left != null && right != null && left.hasRoot() && right.hasRoot()) {
            return root + "\n" +
                    left + "\n  " +
                    right + "\n  ";
        }
        else if (root != -1) {
            return String.valueOf(root);
        }
        else {
            return " ";
        }
    }
}

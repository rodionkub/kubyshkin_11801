package Classwork.ContolWork;

//ВАРИАНТ 1

public class Task02 {
    public static void main(String[] args) {
        int[] a = {12, 3, 4};
        int n = a.length;
        int sum = 0;

        //Дискриминант = 2^2i + 2*i!*2^i + (i!)^2 + 4*a[i]!
        //Он всегда неотрицателен. Значит корень есть при любых a, b, c
        for (int i = 0; i < n; i++) {
            if (isThereEvenDigit(a[i])) {
                while (a[i] != 0) {
                    sum += a[i] % 10;
                    a[i] /= 10;
                }
            }
        }
        System.out.println(sum);
    }
    public static boolean isThereEvenDigit(int x) {
        while (x != 0) {
            if (x % 2 == 0) {
                return true;
            }
            x /= 10;
        }
        return false;
    }

}

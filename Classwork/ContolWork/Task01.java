package Classwork.ContolWork;

//ВАРИАНТ 1

public class Task01 {
    public static void main(String[] args) {
        int count = 0;
        int[] a = {315, 3, 135, 81};
        int n = a.length;

        for (int i = 0; i < n; i++) {
            if (isMultiplyThree(a[i]) && areAllDigitsOdd(a[i])) {
                count++;
            }
        }

        System.out.println(count);
    }
    public static boolean isMultiplyThree(int x) {
        if (x % 3 == 0) {
            return true;
        }
        return false;
    }

    public static boolean areAllDigitsOdd(int x) {
        while (x != 0) {
            if (x % 2 == 0) {
                return false;
            }
            x /= 10;
        }
        return true;
    }
}

package Classwork.ContolWork;

public class HavingFun {
    public static void main(String[] args) {
        int n = 6;
        int[] a = {1, 5, 6, 2, 3, 4};
        int fakeN = n;

        int max = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] > max) {
                max = a[i];
            }
        }
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                if (a[i] - fakeN == 0) {
                    System.out.print("0");
                }
                else {
                    System.out.print(" ");
                }
                fakeN--;
            }
            fakeN = n;
            System.out.println();
        }

    }
}

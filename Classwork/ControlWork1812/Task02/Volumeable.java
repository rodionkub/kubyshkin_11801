package Classwork.ControlWork1812.Task02;

public interface Volumeable {
    double getS();
    double getV();
}

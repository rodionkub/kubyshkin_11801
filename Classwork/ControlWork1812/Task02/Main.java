package Classwork.ControlWork1812.Task02;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Volumeable> objects = new ArrayList<>();
        Volumeable cylinder1 = new Cylinder(2, 3);
        Volumeable cylinder2 = new Cylinder(3, 4);
        Volumeable sphere1 = new Sphere(5);
        Volumeable sphere2 = new Sphere(6);

        objects.add(cylinder1);
        objects.add(cylinder2);
        objects.add(sphere1);
        objects.add(sphere2);

        for (int i = 0; i < objects.size(); i++) {
            System.out.println("Object " + i + " S: " + objects.get(i).getS());
            System.out.println("Object " + i + " V: " + objects.get(i).getV());
        }


    }


}

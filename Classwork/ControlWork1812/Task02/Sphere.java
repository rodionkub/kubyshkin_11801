package Classwork.ControlWork1812.Task02;

public class Sphere implements Volumeable {
    private double r;
    final double p = 3.14;

    public Sphere(double r) {
        this.r = r;
    }


    @Override
    public double getS() {
        return 4 * p * r * r;
    }

    @Override
    public double getV() {
        return 4.0/3 * p * r * r * r;
    }
}



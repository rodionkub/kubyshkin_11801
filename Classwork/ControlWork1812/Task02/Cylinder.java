package Classwork.ControlWork1812.Task02;

public class Cylinder implements Volumeable {
    private double r;
    final double p = 3.14;
    private double h;


    public Cylinder(double r, double h) {
        this.r = r;
        this.h = h;
    }

    @Override
    public double getS() {
        return 2 * p * r * h + 2 * p * r * r;
    }

    @Override
    public double getV() {
        return p * r * r * h;
    }
}

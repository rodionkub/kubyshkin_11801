package Classwork.ControlWork1812.Task01;

// ВАРИАНТ 2

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Appelacion> appelacions = new ArrayList<>();
        ArrayList<Grape> grapes = new ArrayList<>();
        ArrayList<Wine> wines = new ArrayList<>();

        File appelacionFile = new File("/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/ControlWork1812/appellations.csv");
        File grapeFile = new File("/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/ControlWork1812/grapes.csv");
        File wineFile = new File("/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/ControlWork1812/wine.csv");

        Scanner appelacionSc = new Scanner(appelacionFile);
        Scanner wineSc = new Scanner(wineFile);
        Scanner grapeSc = new Scanner(grapeFile);

        appelacionSc.nextLine();
        wineSc.nextLine();
        grapeSc.nextLine();

        while (appelacionSc.hasNextLine()) {
            String line = appelacionSc.nextLine();
            appelacions.add(new Appelacion(
                    Integer.parseInt(line.split(",")[0]),
                    line.split(",")[1],
                    line.split(",")[2],
                    line.split(",")[3],
                    line.split(",")[4],
                    line.split(",")[5]
            ));
        }
        while (grapeSc.hasNextLine()) {
            String line = grapeSc.nextLine();
            grapes.add(new Grape(
                    Integer.parseInt(line.split(",")[0]),
                    line.split(",")[1],
                    line.split(",")[2]
            ));
        }
        while (wineSc.hasNextLine()) {
            String line = wineSc.nextLine();
            wines.add(new Wine(
                    Integer.parseInt(line.split(",")[0]),
                    line.split(",")[1],
                    line.split(",")[2],
                    line.split(",")[3],
                    line.split(",")[4],
                    line.split(",")[5],
                    line.split(",")[6],
                    line.split(",")[7],
                    line.split(",")[8],
                    line.split(",")[9],
                    line.split(",")[10]
            ));
        }

        int amountOfWines = 0;
        int maxAmountOfWines = 0;

        for (int i = 0; i < appelacions.size(); i++) {
            for (int j = 0; j < wines.size(); j++) {
                if (appelacions.get(i).getAppelacion().equals(wines.get(j).getAppelacion())) {
                    amountOfWines++;
                }
            }
            maxAmountOfWines = amountOfWines > maxAmountOfWines ? amountOfWines : maxAmountOfWines;
            amountOfWines = 0;
        }

        System.out.println(maxAmountOfWines);

        amountOfWines = 0;
        Grape tempGrape;
        for (int i = 0; i < grapes.size(); i++) {
            for (int j = 0; j < wines.size(); j++) {
                if (grapes.get(i).getGrape().equals(wines.get(j).getGrape())) {
                    amountOfWines++;
                }
            }
            tempGrape = new Grape(
                    grapes.get(i).getId(),
                    grapes.get(i).getGrape(),
                    grapes.get(i).getColor()
            );
            tempGrape.setAmountOfWines(amountOfWines);
            grapes.set(i, tempGrape);
            System.out.println(grapes.get(i).printAmountOfWines());
            amountOfWines = 0;
        }
    }
}

package Classwork.ControlWork1812.Task01;

public class Grape {
    //ID,Grape,Color
    private int id;
    private String grape;
    private String color;
    private int amountOfWines;

    public Grape(int id, String grape, String color) {
        this.id = id;
        this.grape = grape;
        this.color = color;
    }

    public String getGrape() {
        return grape;
    }

    public int getAmountOfWines() {
        return amountOfWines;
    }

    public void setAmountOfWines(int amountOfWines) {
        this.amountOfWines = amountOfWines;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGrape(String grape) {
        this.grape = grape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String printAmountOfWines() {
        return "Grape: " + grape + ", Amount of wines: " + amountOfWines;
    }
}

package Classwork.ControlWork1812.Task01;

public class Wine {
    //No,Grape,Winery,Appelation,State,Name,Year,Price,Score,Cases,Drink
    private int id;
    private String grape;
    private String winery;
    private String appelacion;
    private String state;
    private String name;
    private String year;
    private String price;
    private String score;
    private String cases;
    private String drink;

    public Wine(int id, String grape, String winery, String appelacion, String state, String name, String year, String price, String score, String cases, String drink) {
        this.id = id;
        this.grape = grape;
        this.winery = winery;
        this.appelacion = appelacion;
        this.state = state;
        this.name = name;
        this.year = year;
        this.price = price;
        this.score = score;
        this.cases = cases;
        this.drink = drink;
    }

    public String getAppelacion() {
        return appelacion;
    }

    public String getGrape() {
        return grape;
    }
}

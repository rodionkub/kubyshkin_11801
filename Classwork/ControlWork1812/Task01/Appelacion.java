package Classwork.ControlWork1812.Task01;

public class Appelacion {
    //No,Appelation,County,State,Area,isAVA
    private int id;
    private String appelacion;
    private String county;
    private String state;
    private String area;
    private String isAVA;

    public Appelacion(int id, String appelacion, String county, String state, String area, String isAVA) {
        this.id = id;
        this.appelacion = appelacion;
        this.county = county;
        this.state = state;
        this.area = area;
        this.isAVA = isAVA;
    }

    public String getAppelacion() {
        return appelacion;
    }
}

package Classwork.ControlWork1203;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Polynom {
    private int coef;
    private int deg;
    ArrayList<Polynom> list = new ArrayList<>();

    public Polynom(int coef, int deg) {
        this.coef = coef;
        this.deg = deg;
    }

    public int getCoef() {
        return coef;
    }

    public int getDeg() {
        return deg;
    }

    public Polynom(String filename) throws FileNotFoundException {
        Scanner in = new Scanner(new File(filename));
        while (in.hasNextLine()) {
            String line = in.nextLine();
            list.add(new Polynom(
                    Integer.parseInt(line.split("-")[0]),
                    Integer.parseInt(line.split("-")[1])));
        }
        sort();
    }

    public void insert(int coef, int deg) {
        int i = 0;
        if (deg != 0) {
            while (list.get(i + 1).getDeg() != deg - 1) {
                i++;
            }
        }
        else {
            list.add(new Polynom(coef, deg));
        }
        list.add(i, new Polynom(coef, deg));
        writeToFile();
    }

    public void delete(int deg) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDeg() == deg) {
                list.remove(i);
                break;
            }
        }
    }

    private void sort() {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i).getDeg() > list.get(j).getDeg()) {
                    Polynom temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        writeToFile();
    }

    private void writeToFile() {
        FileWriter fr = null;
        String data = "";
        for (Polynom aList : list) {
            data = data.concat(aList.getCoef() + "-" +
                    aList.getDeg() + "\n");
        }
        try {
            fr = new FileWriter(Main.filename, false);
            fr.write(data);
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            try {
                assert fr != null;
                fr.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void combine() {
        int deg = list.get(0).getDeg();
        int i = 0;
        while (deg != -1) {
            if (i + 1 == list.size()) {
                break;
            }
            while (list.get(i).getDeg() == list.get(i + 1).getDeg()) {
                list.set(i,
                        new Polynom(list.get(i).getCoef() + list.get(i + 1).getCoef(), deg));
                list.remove(i + 1);
                if (i + 1 == list.size()) {
                    break;
                }
            }
            deg--;
            i++;
        }
        writeToFile();
    }

    public void sum(Polynom p) {
        combine();
        p.combine();
        for (int i = 0; i < this.list.size(); i++) {
            for (int j = 0; j < p.list.size(); j++) {
                if (this.list.get(i).getDeg() == p.list.get(j).getDeg()) {
                    this.list.set(i, new Polynom(this.list.get(i).getCoef() +
                            p.list.get(j).getCoef(), this.list.get(i).getDeg()));
                }
            }
        }
    }

    public void derivate() {
        combine();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDeg() == 0) {
                list.remove(i);
                break;
            }
            list.set(i,
                    new Polynom(list.get(i).getDeg() * list.get(i).getCoef(), list.get(i).getDeg() - 1));
        }
        writeToFile();
    }

    @Override
    public String toString() {
        String toString = "";
        for (int i = 0; i < list.size(); i++) {
            toString = toString.concat(list.get(i).getCoef() + "-" + list.get(i).getDeg() + "\n");
        }
        return toString;
    }
}

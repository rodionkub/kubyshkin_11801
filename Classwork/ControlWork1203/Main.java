package Classwork.ControlWork1203;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static final String filename = "polynoms";
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);
        String input;
        Polynom polynom = new Polynom(filename);
        while(true) {
            input = in.nextLine();
            if (!input.equals("0")) {
                polynom.insert(
                        Integer.parseInt(input.split("-")[0]),
                        Integer.parseInt(input.split("-")[1]));
            }
            else {
                break;
            }
        }
        polynom.derivate();
        System.out.println(polynom);
    }

}

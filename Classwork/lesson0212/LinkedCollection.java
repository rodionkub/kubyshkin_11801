/*package Classwork.lesson0212;

import java.util.Collection;
import java.util.Iterator;

public class LinkedCollection<T> implements Collection<T> {
    private Elem<T> head;

    public LinkedCollection(Elem<T> head) {
        this.head = head;
    }

    @Override
    public int size() { // works
        Elem p = head;
        int count = 0;
        while (p != null) {
            p = p.getNext();
            count++;
        }
        return count;
    }

    @Override
    public boolean isEmpty() { // works
        return this.size() == 0;
    }

    @Override
    public boolean contains(Object o) { // works
        Elem<T> p = head;
        while (p.getValue() != o && p.getNext() != null) {
            p = p.getNext();
        }
        return p.getValue() == o;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() { // works
        Object[] res = new Object[this.size()];
        Elem<T> p = head;
        for (int i = 0; i < res.length; i++) {
            res[i] = p.getValue();
            p = p.getNext();
        }
        return res;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(T t) { // works
        Elem<T> p = head;
        while (p.getNext() != null) {
            p = p.getNext();
            if (p.getValue() == t) {
                return false;
            }
        }
        Elem<T> temp = new Elem<>(t, null);
        p.setNext(temp);
        return true;
    }

    @Override
    public boolean remove(Object o) { // works
        while (head.getValue() == o) {
            head = head.getNext();
        }
        Elem<T> p = head;
        while (p.getNext() != null) {
            if (p.getNext().getValue() == o) {
                p.setNext(p.getNext().getNext());
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) { // works
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) { // works
        for (T x : c) {
            this.add(x);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) { // works
        for (Object o : c) {
            this.remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) { // works
        Elem<T> p = head;
        int size = this.size();
        int count = 0;
        while (p != null) {
            if (!c.contains(p.getValue())) {
                this.remove(p.getValue());
                count++;
            }
            p = p.getNext();
        }
        return count == size - c.size();
    }

    @Override
    public void clear() { // works
        head = null;
    }
}
*/
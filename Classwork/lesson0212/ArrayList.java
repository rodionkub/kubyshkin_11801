package Classwork.lesson0212;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayList<T> implements List<T> {
    private T[] array;

    public ArrayList(T[] array) {
        this.array = array;
    }

    public ArrayList() {}

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return array.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (Object o1 : array) {
            if (o1.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        Object[] res = new Object[array.length + 1];
        int i;
        for (i = 0; i < array.length; i++) {
            res[i] = array[i];
        }
        res[i] = t;
        array = (T[]) res;
        return true;
    }

    @Override
    public boolean remove(Object o) { // works
        Object[] res = new Object[array.length - 1];
        int j = 0;
        boolean flag = false;
        for (T t : array) {
            if (!flag) {
                if (!t.equals(o) && j < res.length) {
                    res[j] = t;
                    j++;
                } else {
                    flag = true;
                }
            }
            else {
                res[j] = t;
                j++;
            }
        }
        array = (T[]) res;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) { // works
        for (Object o: c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) { // works
        for (T t : c) {
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) { // works
        Object[] temp = new Object[array.length + c.size()];
        int i;
        for (i = 0; i < index; i++) {
            temp[i] = array[i];
        }
        int k = i;
        for (T t : c) {
            temp[i] = t;
            i++;
        }
        for (int j = i; j < temp.length; j++) {
            temp[j] = array[k];
            k++;
        }
        array = (T[]) temp;
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            this.remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (T t : array) {
            if (!c.contains(t)) {
                this.remove(t);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        array = null;
    }

    @Override
    public T get(int index) {
        return array[index];
    }

    @Override
    public T set(int index, T element) {
        array[index] = element;
        return element;
    }

    @Override
    public void add(int index, T element) {
        Object[] temp = new Object[array.length + 1];
        int i;
        for (i = 0; i < index; i++) {
            temp[i] = array[i];
        }
        temp[i] = element;
        array = (T[]) temp;
    }

    @Override
    public T remove(int index) {
        Object[] temp = new Object[array.length - 1];
        T element = null;
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (i != index) {
                temp[j] = array[i];
                j++;
            }
            else {
                element = array[i];
            }
        }
        array = (T[]) temp;
        return element;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        while (!array[i].equals(o) && i < array.length - 1) {
            i++;
        }
        return i == array.length - 1 ? -1 : i;
    }

    @Override
    public int lastIndexOf(Object o) {
        int i = array.length - 1;
        while (!array[i].equals(o) && i > 0) {
            i--;
        }
        return i == 0 ? -1 : i;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}

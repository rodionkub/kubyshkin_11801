package Classwork.lesson0212;

import java.util.Collection;
import java.util.Iterator;

public class ArrayCollection<T> implements Collection<T> {
    private T[] array;

    public ArrayCollection(T[] array) {
        this.array = array;
    }


    @Override
    public int size() {
        return array.length;
    } // works

    @Override
    public boolean isEmpty() {
        return array.length == 0;
    } // works

    @Override
    public boolean contains(Object o) { // works
        for (T t : array) {
            if (t.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() { // works
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        // не понял :(
        return null;
    }

    @Override
    public boolean add(T t) { // works
        Object[] res = new Object[array.length + 1];
        int i;
        for (i = 0; i < array.length; i++) {
            res[i] = array[i];
        }
        res[i] = t;
        array = (T[]) res;
        return true;
    }

    @Override
    public boolean remove(Object o) { // works
        Object[] res = new Object[array.length - 1];
        int j = 0;
        for (T t : array) {
            if (!t.equals(o)) {
                res[j] = t;
                j++;
            }
        }
        array = (T[]) res;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) { // works
        for (Object o: c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) { // works
        for (T t : c) {
            this.add(t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) { // works
        for (Object o : c) {
            this.remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) { // works
        for (T t : array) {
            if (!c.contains(t)) {
                this.remove(t);
            }
        }
        return true;
    }

    @Override
    public void clear() { // works
        array = null;
    }
}

package Classwork.lesson0212;

public class Table {
    ArrayList<Integer> numbers = new ArrayList<>();
    ArrayList<String> animals = new ArrayList<>();
    ArrayList<String> items = new ArrayList<>();

    public Table() {
    }

    public ArrayList<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(ArrayList<Integer> numbers) {
        this.numbers = numbers;
    }

    public ArrayList<String> getAnimals() {
        return animals;
    }

    public void setAnimals(ArrayList<String> animals) {
        this.animals = animals;
    }

    public ArrayList<String> getItems() {
        return items;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }


}

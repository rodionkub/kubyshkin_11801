package Classwork.lesson0211;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    private static int[] a = {3, 3, 2};
    private static Elem p;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Elem head = null;

        for (int i = 0; i < a.length; i++) {
            p = new Elem(a[i], head);
            head = p;
        }

        assert head != null;
        p = removeFirstValue(head, 2);
        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }

    private static Elem insertBeforeAfterK(Elem head, int m, int k) {
        p = head;
        if (head.getValue() == k) {
            Elem prev = new Elem(m, head);
            Elem next = new Elem(m, head.getNext());
            head.setNext(next);
            head = prev;
            return head;
        }
        // для работы со ВСЕМИ элементами = k, раскомментируй строчки с циклом
        //while (p.getNext().getNext() != null) {
        while (p.getNext().getValue() != k) {
            p = p.getNext();
        }
        Elem prev = new Elem(m, p.getNext());
        p.setNext(prev);
        p = prev.getNext();
        Elem next = new Elem(m, p.getNext());
        p.setNext(next);
        //}
        return head;
    }

    private static int findMax(Elem head) {
        p = head;
        int max = 0;
        while (p != null) {
            if (p.getValue() > max) {
                max = p.getValue();
            }
            p = p.getNext();
        }
        return max;
    }

    private static boolean findValue(Elem head, int k) {
        p = head;
        while (p != null) {
            if (p.getValue() == k) {
                return true;
            }
            p = p.getNext();
        }
        return false;
    }

    private static Elem removeLast(Elem head) {
        p = head;
        while (p.getNext().getNext() != null) {
            p = p.getNext();
        }
        p.setNext(null);
        return head;
    }

    private static Elem removeFirst(Elem head) {
        return head.getNext();
    }

    private static Elem removePreLast(Elem head) {
        p = head;
        while (p.getNext().getNext().getNext() != null) {
            p = p.getNext();
        }
        p.setNext(p.getNext().getNext());
        return head;
    }

    private static Elem removeFirstValue(Elem head, int k) {
        if (head.getValue() == k) {
            head = head.getNext();
            return head;
        }
        p = head;
        boolean flag = false;
        while (p.getNext().getValue() != k && p.getNext().getNext() != null) {
            if (p.getNext().getValue() == k) {
                flag = true;
                break;
            }
            p = p.getNext();
        }
        if (flag || p.getNext().getValue() == k) {
            p.setNext(p.getNext().getNext());
        }
        return head;
    }

    private static Elem removeAllValue(Elem head, int k) {
        while (head.getValue() == k) {
            if (head.getNext() != null) {
                head = head.getNext();
            }
            else {
                return new Elem(0, null);
            }
        }
        p = head;
        while (p.getNext() != null) {
            if (p.getNext().getValue() != k) {
                p = p.getNext();
            }
            else {
                p.setNext(p.getNext().getNext());
            }
        }
        return head;
    }
}

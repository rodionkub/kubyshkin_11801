package Classwork.lesson0415;

import java.util.ArrayList;

public class Main {
    private static Elem p;
    public static int[] a = {1, 2, 0, 3, 0};

    public static void main(String[] args) {
        Elem head = setHead(a);
        Elem r = removeAllValue(head,0);
        System.out.println(r.getValue());
        while (r.getNext() != null) {
            r = r.getNext();
            System.out.println(r.getValue());
        }
    }

    public static Elem setHead(int[] a) {
        Elem head = null;

        for (int i = 0; i < a.length; i++) {
            p = new Elem(a[i], head);
            head = p;
        }

        return head;
    }

    public static ArrayList elemsToArray(Elem head) {
        ArrayList<Integer> result = new ArrayList<>();
        Elem p = head;
        result.add(p.getValue());
        while (p.getNext() != null) {
            p = p.getNext();
            result.add(p.getValue());
        }
        return result;
    }

    public static Elem removeAllValue(Elem head, int k) {
        while (head.getValue() == k) {
            if (head.getNext() != null) {
                head = head.getNext();
            }
            else {
                return new Elem(0, null);
            }
        }
        p = head;
        while (p.getNext() != null) {
            if (p.getNext().getValue() != k) {
                p = p.getNext();
            }
            else {
                p.setNext(p.getNext().getNext());
            }
        }
        return head;
    }
}

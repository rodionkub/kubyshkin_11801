package Classwork.lesson0415;

import org.junit.Assert;
import Classwork.lesson0409.*;
import java.util.ArrayList;

public class Test {

    @org.junit.Test
    public void checkRemoval() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(2);
        list.add(1);
        Assert.assertEquals(list, Main.elemsToArray(Main.removeAllValue(Main.setHead(Main.a), 0)));
    }

    @org.junit.Test
    public void checkHalfSum() {
        int[] arr = {1, 2, 3, 4, 10, 51, 2, 17};
        Assert.assertEquals(90, Task01.sum(arr));
    }
}

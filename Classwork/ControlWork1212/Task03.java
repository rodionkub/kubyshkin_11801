package Classwork.ControlWork1212;

// ВАРИАНТ 2

public class Task03 {
    static int firstWordIndex;
    static int secondWordIndex;

    public static void main(String[] args) {

        boolean firstWordFound = false;
        boolean secondWordFound = false;
        String[] strings = {"mama", "papa", "help", "meee"};
        char[][] chars = {
                {'m', 'a', 'm', 'a'},
                {'p', 'a', 'p', 'a'},
                {'b', 'r', 'a', 't'}
        };

        for (int i = 0; i < strings.length; i++) {
            if (searchForWord(strings[i], chars) != -1 && !firstWordFound) {
                firstWordIndex = searchForWord(strings[i], chars);
                firstWordFound = true;
            }
            else if (searchForWord(strings[i], chars) != -1 && !secondWordFound) {
                secondWordIndex = searchForWord(strings[i], chars);
                secondWordFound = true;
            }
        }
        if (firstWordIndex < secondWordIndex) {
            System.out.println("All good, buddy");
        }
        else {
            System.out.printf("Nah, I'm sorry, dude");
        }
    }

    public static int searchForWord(String word, char[][] chars) {
        int count = 0;
        for (int countLine = 0; countLine < chars.length; countLine++) {
            if (word.length() == chars[countLine].length) {
                for (int j = 0; j < word.length(); j++) {
                    if (word.charAt(j) == chars[countLine][j]) {
                        count++;
                    }
                    if (count == word.length()) {
                        return countLine;
                    }
                }
            }
            count = 0;
        }
        return -1;
    }

}

package Classwork.ControlWork1212;

// ВАРИАНТ 2

public class Task01 {
    public static void main(String[] args) {
        int n;
        n = 5;
        int a[] = {3, 2, 3, 6, 1};
        int max = a[0];
        int min = a[0];

        for (int i = 0; i < n; i++) {
            max = a[i] > max ? a[i] : max;
            min = a[i] < min ? a[i] : min;
        }

        max = max >= n ? n - 1 : max;

        a = flipArray(a, min, max);

        for (int i = 0; i < n; i++) {
            System.out.println(a[i] + " ");
        }

    }

    public static int[] flipArray(int[] a, int min, int max) {

        for (int i = min; i < max; i++) {
            int t;
            t = a[i];
            a[i] = a[max];
            a[max] = t;
            max--;
        }
        return a;
    }
}

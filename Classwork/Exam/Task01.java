package Classwork.Exam;
//length() charAt()

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        final int n = in.nextInt();
        int[] nums = new int[n];
        String[] strs = new String[n];
        for (int i = 0; i < n; i++) {
            nums[i] = in.nextInt();
        }
        for (int i = 0; i < n; i++) {
            strs[i] = in.next();
        }
        int sum;
        int amount;
        int count = 0;
        boolean flag = false;
        for (int i = 0; i < n && !flag; i++) {
            sum = requiredSumOfString(strs[i]);
            amount = amountOfOddDigits(sum);
            if(isItThereTwice(nums, amount)) {
                count++;
            }
            if (count == 3) {
                flag = true;
            }
        }
        if (flag) {
            System.out.println("YES!!!");
        }
        else {
            System.out.println("Im sorry ma man");
        }
    }

    public static int requiredSumOfString(String str) {
        int length = str.length();
        int i = 0;
        int sum = 0;
        while (i < length) {
            sum += Integer.parseInt(String.valueOf(str.charAt(i)));
            i += 2;
        }
        return sum;
    }

    public static int amountOfOddDigits(int x) {
        int amount = 0;
        while (x > 0) {
            if (x % 2 != 0) {
                amount++;
            }
            x /= 10;
        }
        return amount;
    }

    public static boolean isItThereTwice(int[] nums, int amount) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == amount) {
                count++;
            }
            if (count == 2) {
                return true;
            }
        }
        return false;
    }


}

package Classwork.Exam.Task02;

public class BlackHole implements Destroying, SelfCreating {
    private double r;
    private double age;
    private boolean closeToDeath;

    public BlackHole(double r, double age) {
        this.r = r;
        this.age = age;
    }

    public BlackHole(double r, double age, boolean closeToDeath) {
        this(r, age);
        this.closeToDeath = closeToDeath;

    }

    @Override
    public void destroySomething() {
        System.out.println("I ABSORB YOU");
    }

    @Override
    public void comeOutOfNowhere() {
        System.out.println("HAHA, HAVEN'T EXPECTED, YOU FOOL?");
    }
}

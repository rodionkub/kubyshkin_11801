package Classwork.Exam.Task02;

public class Mercury extends Planet {
    private double R;
    private double age;
    private boolean isLiveable;
    private String[] sattelites;

    public Mercury(double r, double age) {
        R = r;
        this.age = age;
    }

    public Mercury(double r, double age, boolean isLiveable) {
        this(r, age);
        this.isLiveable = isLiveable;
    }


    @Override
    public void rotate() {
        System.out.println("Rotating quietly around Sun");
    }

    @Override
    public void killHuman() {
        System.out.println("Try to breath lol");
    }

    @Override
    public void satellitesToString(String[] satellites) {
        super.satellitesToString(satellites);
    }


}

package Classwork.Exam.Task02;

public class Earth extends Planet {
    private double r;
    private double age;
    private boolean isLiveable;
    private String[] sattelites;

    public Earth(double r, double age, boolean isLiveable) {
        this.r = r;
        this.age = age;
        this.isLiveable = isLiveable;
    }

    public Earth(double r, double age, boolean isLiveable, String[] sattelites) {
        this(r, age, isLiveable);
        this.sattelites = sattelites;
    }

    @Override
    public void rotate() {
        System.out.println("Rotating quietly around Sun");
    }

    @Override
    public void killHuman() {
        System.out.println("Casting Volcano...");
    }

    @Override
    public void satellitesToString(String[] satellites) {
        super.satellitesToString(satellites);
    }
}

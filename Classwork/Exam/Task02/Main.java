package Classwork.Exam.Task02;

import java.util.ArrayList;

public class Main {
    private static ArrayList<Destroying> threats = new ArrayList<>();
    private static ArrayList<Planet> planets = new ArrayList<>();

    public static void main(String[] args) {
        setArrayList();
        setArrayList2();

        for (int i = 0; i < threats.size(); i++) {
            threats.get(i).destroySomething();
            System.out.println();
        }

        for (int i = 0; i < threats.size(); i++) {
            planets.get(i).rotate();
            planets.get(i).killHuman();
            System.out.println();
        }

    }

    static void setArrayList() {
        Destroying blackHole = new BlackHole(15.7, 3.2);
        Destroying comet = new Comet(0.02, 400, "Chelyabinsk");
        threats.add(blackHole);
        threats.add(comet);
    }

    static void setArrayList2() {
        Planet earth = new Earth(6.3, 4.54, true);
        Planet mercury = new Mercury(2.4,4.6, false);
        planets.add(earth);
        planets.add(mercury);
    }
}

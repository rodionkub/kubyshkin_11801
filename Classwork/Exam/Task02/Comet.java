package Classwork.Exam.Task02;

public class Comet implements Destroying {
    private double r;
    private double speed;
    private String probableHitPoint;

    public Comet(double r, double speed) {
        this.r = r;
        this.speed = speed;
    }

    public Comet(double r, double speed, String probableHitPoint) {
        this(r, speed);
        this.probableHitPoint = probableHitPoint;
    }


    @Override
    public void destroySomething() {
        System.out.println("Wait for it, wait for it!..");
        System.out.println("On my way");
    }

    public void burn() {
        System.out.println("And I'm fine with that.");
    }
}

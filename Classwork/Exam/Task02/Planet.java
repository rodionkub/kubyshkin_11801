package Classwork.Exam.Task02;

public abstract class Planet {
    public abstract void rotate();
    public abstract void killHuman(); //даже Земля может убивать по идее
    public void satellitesToString(String[] satellites) {
        int n = satellites.length;
        for (int i = 0; i < n; i++) {
            System.out.println(satellites[i] + " ");
        }
    }
}

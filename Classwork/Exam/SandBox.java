package Classwork.Exam;

public class SandBox {
    public static void main(String[] args) {
        int n = 3;
        int[] arr = {-1, -2, 3};

        System.out.println(f(arr, n, 0, 0));

    }

    public static int f(int[] a, int n, int count, int i) {
        if (n > 0) {
            if(a[i] < 0) {
                count++;
                i++;
                return f(a, n, count, i);
            }
        }
        else {
            return count;
        }
        return count;
    }
}

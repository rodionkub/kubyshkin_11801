package Classwork.lesson0409;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyThread03 extends Thread {
    private ArrayList<String> links;
    private int n;
    private int k;
    private String extension;

    public MyThread03(ArrayList<String> links, int n, int k, String extension) {
        this.links = links;
        this.n = n;
        this.k = k;
        this.extension = extension;
    }

    @Override
    public void run() {
        System.out.println("Starting downloading files from " + k + " to " + n);
        for (int i = k; i < n; i++) {
            try {
                downloadBinaryFile(links.get(i), "/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/lesson0409/res/file" + i + "." + extension);
                System.out.println("file" + i + " must be saved");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void downloadBinaryFile(String urlPath, String path) throws IOException, MalformedURLException {
        URL url = new URL(urlPath);
        InputStream reader = url.openStream();
        FileOutputStream writer = new FileOutputStream(path);
        int elem;
        while((elem = reader.read()) != -1) {
            writer.write(elem);
            writer.flush();
        }
        reader.close();
        writer.close();
    }


}

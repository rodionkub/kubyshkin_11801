package Classwork.lesson0409;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task05 {
    private static final String site = "https://stat.kpfu.ru/sveden/document/";
    private static ArrayList<String> links = new ArrayList<>();

    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();
        int noMore = in.nextInt();

        parseAllLinks(site, "pdf");
        System.out.println("Total files: " + links.size());
        for (int i = 0; i < k; i++) {
            int a = links.size() / k * i;
            int b = links.size() / k * (i + 1) - 1;

            if (MyThread03.activeCount() < noMore) {
                new MyThread03(links, b, a, "pdf").start();
            }
            else {
                new MyThread03(links, b, a, "pdf").join();
            }
        }
    }

    private static void parseAllLinks(String urlPath, String extension) throws IOException {
        String reg = "\"https?.*/.*\\." + extension + "\"";
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher;

        URL url = new URL(urlPath);
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        while((line = reader.readLine()) != null) {
            matcher = pattern.matcher(line);
            while(matcher.find()) {
                String s = matcher.group().split("\"")[1];
                links.add(s);
            }
        }
        reader.close();
    }
}


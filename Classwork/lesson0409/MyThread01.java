package Classwork.lesson0409;

public class MyThread01 extends Thread {
    private int sum = 0;
    private int[] arr;
    private int halfNumber;

    @Override
    public void run() {
        int k;
        if (halfNumber == 1) {
            k = 0;
        }
        else {
            if (arr.length % 2 == 0) {
                k = arr.length / 2;
            }
            else {
                k = arr.length / 2 + 1;
            }
        }
        for (int i = k; i < k + arr.length / 2; i++) {
            sum += arr[i];
        }
    }

    public void setArray(int[] arr, int halfNumber) {
        this.arr = arr;
        this.halfNumber = halfNumber;
    }

    public int getSum() {
        return sum;
    }
}

package Classwork.lesson0409;

import java.util.Scanner;

public class Task02 {
    private static final String PATH_TO_FILE = "/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/lesson0409/test_screen.png";
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            new MyThread02(PATH_TO_FILE, i).start();
            System.out.println(MyThread02.activeCount());
        }
    }
}

package Classwork.lesson0409;

import java.io.*;

public class MyThread02 extends Thread {
    private String path;
    private int k;

    public MyThread02(String path, int k) {
        this.path = path;
        this.k = k;
    }

    @Override
    public void run() {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(new File(path));
            fos = new FileOutputStream(new File("/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/lesson0409/res/test" + this.k + ".png"));
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert fis != null;
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                assert fos != null;
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

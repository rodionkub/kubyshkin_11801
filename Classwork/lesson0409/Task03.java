package Classwork.lesson0409;

import java.util.Scanner;

public class Task03 {
    private static final String PATH_TO_FILE = "/Users/rodionkub/IdeaProjects/lesson5.09/src/Classwork/lesson0409/test_screen.png";
    public static void main(String[] args) throws InterruptedException {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        for (int i = 0; i < n; i++) {
            if (MyThread02.activeCount() < k) {
                new MyThread02(PATH_TO_FILE, i).start();
            }
            else {
                new MyThread02(PATH_TO_FILE, i).join();
            }
            System.out.println(MyThread02.activeCount());
        }
    }
}

package Classwork.lesson0110;

public class Task06 {
    public static void main(String[] args) {
        int[] array = {13, 23, 1463, 141, 3};

        System.out.println(hasDigitThree(array));
    }

    public static boolean hasDigitThree(int[] a) {
        int count = 0;
        int n = a.length;

        for (int x : a) {
            while (x != 0) {
                if (isThree(x)) {
                    count++;
                    x = 0;
                }
                x /= 10;
            }
        }
        return count == n;
    }

    public static boolean isThree(int x) {
        return x % 10 == 3;
    }
}

package Classwork.lesson0110;

public class Task03 {
    public static void main(String[] args) {
        int n = 42678;

        System.out.println(checkIfThereIsUneven(n));
    }
    public static boolean checkIfThereIsUneven(int n) {
        while (n != 0) {
            if (ifDigitIsUneven(n)) {
                return true;
            }
            n /= 10;
        }
        return false;
    }
    public static boolean ifDigitIsUneven(int n) {
        return (n % 10) % 2 != 0;
    }
}

package Classwork.lesson0110;

public class Task01 {
    public static void main(String[] args) {
        int[] array = {1, 5, 3, 7};
        System.out.println(hasEvenElement(array));
    }
    public static boolean hasEvenElement(int[] array) {
        for (int x : array) {
            if (isEven(x)) {
                return true;
            }
        }
        return false;
    }
    public static boolean isEven(int x) {
        return x % 2 == 0;
    }
}

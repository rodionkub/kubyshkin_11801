package Classwork.lesson0110;

public class Task05 {
    public static void main(String[] args) {
        int k = 2;
        int[] array = {5, 5, 1, 2, 3};
        boolean ans = false;
        if (numberOfMultipleFive(array) == k) {
            ans = true;
        }
        System.out.print(ans);
    }

    public static int numberOfMultipleFive(int[] a) {
        int count = 0;
        for (int x : a) {
            if (isMultipleFive(x)) {
                count++;
            }

        }
        return count;
    }

    public static boolean isMultipleFive(int x) {
        return x % 5 == 0;
    }
}

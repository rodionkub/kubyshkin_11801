package Classwork.lesson0110;

public class Task02 {
    public static void main(String[] args) {
        int[] array = {1, -3, 4, 5};
        int n = array.length;
        boolean ans = true;
        int i = 0;

        while (i < n - 1 && ans) {
            if (array[i + 1] > 0 && array[i] % 2 == 0) {
                ans = false;
            }
            i++;
        }

        System.out.println(ans);
    }
}

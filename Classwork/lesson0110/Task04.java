package Classwork.lesson0110;

public class Task04 {
    public static void main(String[] args) {
        int[] array = {1, 5, 3, 4, 5};
        int n = array.length;
        int k = 3;
        int m = 2;
        int count = 0;
        boolean ans = true;
        int i = 0;

        while (i < n && ans) {
            if (array[i] > m) {
                count++;
                if (count > k) {
                    ans = false;
                }
            }
            i++;
        }

        System.out.print(ans);
    }
}

package BackTracking;

import Classwork.lesson0212.ArrayList;

import java.util.Arrays;

public class Main {
    private static int n = 24;
    private static Person[] peopleArray = {
            new Person("Diana", "female"),
            new Person("Sasha A", "female"),
            new Person("Rodion", "male"),
            new Person("Ksyusha", "female"),
            new Person("Timur", "male"),
            new Person("Semen", "female"),
            new Person("Syumbel", "female"),
            new Person("Egor", "male"),
            new Person("Kirill", "male"),
            new Person("Niyaz", "male")
    };

    private static ArrayList<Person> comps = new ArrayList<>(peopleArray);

    public static void main(String[] args) {
        ArrayList<Person> s = new ArrayList<>(new Person[]{peopleArray[0]});
        System.out.println(Arrays.toString(backTracking(s, comps).toArray()));
    }

    private static ArrayList<Person> backTracking(ArrayList<Person> s, ArrayList<Person> people) {
        if (s.size() == n) {
            return s;
        } else {
            for (int i = 0; i < people.size(); i++) {
                s.add(people.get(i));
                Person p = people.get(i);
                if (correct(s)) {
                    people.remove(p);
                    backTracking(s, people);
                    people.add(p);
                }
                s.remove(p);
            }


        }
        return new ArrayList<>();
    }


    private static boolean correct(ArrayList<Person> s) {
        System.out.println("Checking " + Arrays.toString(s.toArray()));
        int countMale = 0;
        int countFemale = 0;
        for (int i = 0; i < s.size(); i++) {
            if (s.get(i).sex.equals("female")) {
                countFemale++;
                countMale = 0;
            } else if (s.get(i).sex.equals("male")) {
                countMale++;
                countFemale = 0;
            }
            if (countMale == 2 || countFemale == 2) {
                return false;
            }
        }
        return true;
    }
}

package Project;


import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.table.TableColumn;
import java.io.IOException;
import java.lang.String;
import javax.imageio.ImageIO;
import java.io.File;

public class SimpleWindow extends JFrame {

    JTable table;
    String data[][];
    int sumFood = 0;
    int sumStudy = 0;
    int sumTransport = 0;
    int sumClothes = 0;
    int sumOther = 0;
    int ostText = 0;
    public SimpleWindow() throws IOException {
        super("Project [code: 00]");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);

        JPanel Income = new JPanel();
        JPanel Expense = new JPanel();
        JPanel Analysis = new JPanel();

        JLabel salaryInputText = new JLabel("<html><b>Enter your salary<b></html>");
        String[][] data = {
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
                {"", "", "", "", ""},
        };
        Income.add(salaryInputText);

        JTextArea textArea = new JTextArea(1,10);
        textArea.setLayout(new BoxLayout(textArea,BoxLayout.Y_AXIS));
        textArea.setPreferredSize(new Dimension(200,10));
        textArea.setMinimumSize(new Dimension(200,10));
        textArea.setMaximumSize(new Dimension(300,10));
        textArea.setLayout(new BorderLayout(10,450));
        Income.add(textArea, BorderLayout.CENTER);

        JButton doneButton = new JButton("<html><b>Done</b></html>");
        doneButton.setPreferredSize(new Dimension(500, 20));
        Income.add(doneButton, BorderLayout.CENTER);
        //Shows input salary
        JLabel salaryOutputText = new JLabel("<html><b>Current salary: </b></html>");
        Income.add(salaryOutputText, BorderLayout.CENTER);
        //Needed for /30 calculation
        JLabel salaryOutput = new JLabel();
        Income.add(salaryOutput, BorderLayout.CENTER);
        //Button for the calculation, Income panel
        doneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                salaryOutput.setText(textArea.getText());

                String salary = textArea.getText();

                /*int salaryInt = Integer.parseInt(salary);

                int testingInt = salaryInt/30;

                String testingString = Integer.toString(testingInt);

                JLabel testingStringOutput = new JLabel(testingString);
                Expense.add(testingStringOutput);*/
            }
        });

        //Testing out Category-add feature

        //JFrame categoryFrame = new JFrame();
        //JPanel categoryPanel = new JPanel();

        //categoryFrame.setSize(470,480);

        //categoryFrame.add(categoryPanel);

        //JButton addCategoryButton = new JButton("Manage your categories...");
        //addCategoryButton.setPreferredSize(new Dimension(200, 20));
        //Expense.add(addCategoryButton, BorderLayout.EAST);

        /*String[][] categoryData = {
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
                {""},
        };
        String[] categoryColumnNames = {"Categories"};
        JTable categoryTable = new JTable(categoryData, categoryColumnNames);

        JScrollPane categoryScrollPane = new JScrollPane(categoryTable);
        categoryPanel.add(categoryScrollPane);

        addCategoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                categoryFrame.setVisible(true);
            }
        });

        JButton categoryDoneButton = new JButton("Done");
        categoryDoneButton.setPreferredSize(new Dimension(200,20));
        categoryPanel.add(categoryDoneButton);

        categoryPanel.setBorder(new EmptyBorder(0,0,100,0));
        categoryPanel.setLayout(new BoxLayout(categoryPanel,BoxLayout.Y_AXIS));

        categoryPanel.setLayout(new FlowLayout());

        String[] categoryList = {
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
        };*/

        //Data for the table
        String[] columnNames = {
                "<html><b>Expense</b></html>",
                "<html><b>Category</b></html>",
                "<html><b>Day</b></html>",
                "<html><b>Month</b></html>",
                "<html><b>Description</b></html>"
        };


        //Income panel size control
        Income.setBorder(new EmptyBorder(50,50,50,50));
        //Table size control
        Expense.setBorder(new EmptyBorder(0,0,350,0));
        Expense.setLayout(new BoxLayout(Expense,BoxLayout.Y_AXIS));
        //Data table
        table = new JTable(data, columnNames);

        table.getColumnModel().getColumn(0).setMaxWidth(65);
        table.getColumnModel().getColumn(1).setMaxWidth(100);
        table.getColumnModel().getColumn(2).setMaxWidth(35);
        table.getColumnModel().getColumn(3).setMaxWidth(70);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        //JScrollPane for the table
        JScrollPane scrollPane = new JScrollPane(table);
        Expense.add(scrollPane);
        //Button for calculation and analysis
        JButton analyzeData = new JButton("<html><b>Calculate</b></html>");
        analyzeData.setPreferredSize(new Dimension(500, 20));
        Expense.add(analyzeData, BorderLayout.CENTER);

        JLabel totalWaste = new JLabel("<html><b>Total waste: </b></html>");
        Expense.add(totalWaste);

        JLabel sumOutput = new JLabel("0");
        Expense.add(sumOutput, BorderLayout.CENTER);

        JLabel balanceText = new JLabel("<html><b>Current balance: </b></html>");
        Expense.add(balanceText, BorderLayout.CENTER);

        JLabel balance = new JLabel("0");
        Expense.add(balance, BorderLayout.CENTER);

        String[] categoryNames = {" ", "Food", "Transport", "Study", "Clothes", "Other"};
        JComboBox comboBox = new JComboBox(categoryNames);
        TableColumn categoryColumn = table.getColumnModel().getColumn(1);
        categoryColumn.setCellEditor(new DefaultCellEditor(comboBox));

        String[] monthList = {
                " ",
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
        };

        JComboBox monthComboBox = new JComboBox(monthList);
        TableColumn monthColumn = table.getColumnModel().getColumn(3);
        monthColumn.setCellEditor(new DefaultCellEditor(monthComboBox));

        /*categoryDoneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int k;
                int count = 0;

                while (!categoryData[count][0].equals("")) {
                    count++;

                }

                categoryFrame.setVisible(false);

                comboBox.removeAllItems();

                for (k = 0; k < count; k++) {
                    categoryList[k] = categoryData[k][0];
                    comboBox.addItem(categoryList[k]);
                }

                for (k = 0; k < count; k++) {
                    categoryList[k] = "";
                }

            }
        });*/

        String[][] analysisData = {
                {"Food", ""},
                {"Transport", ""},
                {"Study", ""},
                {"Clothes", ""},
                {"Other", ""},
                {"Amount to spend per day", ""},
        };
        String[] analysisColumnNames = {"Categories", "Money"};
        JTable analysisTable = new JTable(analysisData, analysisColumnNames);

        JScrollPane analysisScrollPane = new JScrollPane(analysisTable);

        Analysis.add(analysisScrollPane);

        //Calculate button click
        analyzeData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int sum = 0;
                int i = 0;

                while (!data[i][0].equals("")) {
                    sum += Integer.parseInt(data[i][0].trim());
                    i++;
                }

                //Changing the Total waste
                sumOutput.setText(Integer.toString(sum));
                int balanceInt = Integer.parseInt(salaryOutput.getText()) - Integer.parseInt(sumOutput.getText());
                balance.setText(Integer.toString(balanceInt));


                int k=0;
                int max=0;
                while (data[k][1] != "") {
                    if (data[k][1]=="Food") {
                        sumFood=Integer.parseInt(data[k][0])+sumFood;
                    }

                    else if (data[k][1]=="Study") {
                        sumStudy=Integer.parseInt(data[k][0])+sumStudy;
                    }

                    else if (data[k][1]=="Transport") {
                        sumTransport=Integer.parseInt(data[k][0])+sumTransport;
                    }

                    else if (data[k][1]=="Clothes") {
                        sumClothes=Integer.parseInt(data[k][0])+sumClothes;
                    }

                    else if (data[k][1]=="Other") {
                        sumOther=Integer.parseInt(data[k][0])+sumOther;
                    }
                    if (Integer.parseInt(data[k][2]) > max){
                        max=Integer.parseInt(data[k][2]);
                    }
                    k++;
                }
                k = 0;


                ostText = Integer.parseInt(balance.getText()) / (31-max);


                analysisData[0][1] = Integer.toString(sumFood);
                analysisData[1][1] = Integer.toString(sumTransport);
                analysisData[2][1] = Integer.toString(sumStudy);
                analysisData[3][1] = Integer.toString(sumClothes);
                analysisData[4][1] = Integer.toString(sumOther);
                analysisData[5][1] = Integer.toString(ostText);
                sumFood = 0;
                sumTransport = 0;
                sumStudy = 0;
                sumClothes = 0;
                sumOther = 0;
                ostText = 0;

            }
        });

        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setToolTipText("Click for combo box");
        categoryColumn.setCellRenderer(renderer);


        Expense.setLayout(new FlowLayout());








        //Adding the tabs
        tabbedPane.add("Income", Income);
        tabbedPane.add("Expense", Expense);
        tabbedPane.add("Analysis", Analysis);
        //God tell me why is this here
        getContentPane().add(tabbedPane, BorderLayout.CENTER);
        tabbedPane.setBackground(Color.BLACK);

        setSize(600, 800);
    }


}

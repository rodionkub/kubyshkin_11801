package Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class LineNumberReader extends BufferedReader {
    private BufferedReader in;
    private int lineNumber;

    public LineNumberReader(Reader in, int sz) {
        super(in, sz);
        lineNumber = 0;
    }


    public String readLine() throws IOException {
        lineNumber++;
        return in.readLine();
    }

    public long skip(long n) throws IOException {
        long count = 0;
        for (int i = 0; i < n; i++) {
            in.read();
            count++;
        }
        return count;
    }

    public int read() throws IOException {
        return in.read();
    }

    public void setLineNumber(int lineNumber) throws IOException {
        while (this.lineNumber != lineNumber) {
            in.readLine();
        }
    }
}

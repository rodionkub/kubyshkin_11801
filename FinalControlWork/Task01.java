package FinalControlWork;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        int ThreeKgAmount, OneKgAmount, count;
        Scanner in = new Scanner(System.in);
        count = 0;
        ThreeKgAmount = in.nextInt();
        OneKgAmount = in.nextInt();
        count += ThreeKgAmount * 3;
        int i = 0;
        while (OneKgAmount > 0 && i < ThreeKgAmount) {
            OneKgAmount -= 2;
            i++;
        }
        count += OneKgAmount / 2 + OneKgAmount % 2;
        System.out.println(count);
    }
}

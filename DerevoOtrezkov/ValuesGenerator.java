package DerevoOtrezkov;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class ValuesGenerator {
    public static void main(String[] args) throws IOException {
        // изначально n = 1000, далее увеличивается на 100 до 10000
        int n = 100;
        while (n != 10100) {
            // создается файл с названием data{n}, где n - количество рёбер
            File file = new File("src/DerevoOtrezkov/data/data" + n);
            file.createNewFile();

            Random random = new Random();
            String output = "";

            // создаем n случайных целых чисел
            for (int i = 0; i < n; i++) {
                output = output.concat(String.valueOf(random.nextInt(100)) + " ");
            }

            System.out.println("Сгенерирован файл с " + n + " чисел");
            // записываем сгенерированные данные в файл data{n}
            writeCollectedData(output, "src/DerevoOtrezkov/data/data" + n, false);

            n += 100;
        }
    }

    public static void writeCollectedData(String output, String filename, boolean append) {
        FileWriter fr = null;
        try {
            fr = new FileWriter(filename, append);
            fr.write(output);
            System.out.println("Запись успешна. \n");
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            try {
                assert fr != null;
                fr.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}


package DerevoOtrezkov;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);
        for (int i = 100; i < 10100; i += 100) {
            Scanner readFile = new Scanner(new File("src/DerevoOtrezkov/data/data" + i));
            String input = readFile.nextLine();
            String[] t = input.split(" ");
            Random random = new Random();
            int[] a = new int[t.length];
            for (int j = 0; j < t.length; j++) {
                a[j] = Integer.parseInt(t[j]);
            }
            Tree tree = new Tree(a);

            double[] check = new double[3];
            for (int j = 0; j < 3; j++) {
                double start = System.nanoTime();
                tree.removeByIndex(i / 2);
                double end = System.nanoTime() - start;
                check[j] = end;
            }
            double one = Math.round((check[0] + check[1] + check[2]) / 3);
            for (int j = 0; j < 3; j++) {
                double start = System.nanoTime();
                tree.add(i / 2, 1);
                double end = System.nanoTime() - start;
                check[j] = end;
            }
            double two = Math.round((check[0] + check[1] + check[2]) / 3);

            for (int j = 0; j < 3; j++) {
                double start = System.nanoTime();
                tree.update(i / 2, 1);
                double end = System.nanoTime() - start;
                check[j] = end;
            }
            double three = Math.round((check[0] + check[1] + check[2]) / 3);

            String data = i + "," + one + "," + two + "," + three + "\n";
            // ValuesGenerator.writeCollectedData(data, "src/DerevoOtrezkov/results", true);
            System.out.println(printData());
        }
    }

    private static String printData() throws FileNotFoundException {
        String s = "";
        Scanner in;
        for (int i = 100; i < 10100; i+=100) {
            in = new Scanner(new File("src/DerevoOtrezkov/data/data" + i));
            while (in.hasNextLine()) {
                s = s.concat(in.nextLine() + "\n");
            }
        }
        return s;
    }
}

package DerevoOtrezkov;

import java.util.Arrays;

public class Tree {
    private int[] array;
    private int[] treeArray;
    private int n;

    public Tree(int[] array) {
        this.array = array;
        this.n = array.length;
        this.treeArray = new int[4 * this.n];
        build(this.array, 1, 0, this.n - 1);
    }

    private void build (int[] array, int v, int treeLeft, int treeRight) {
        if (treeLeft == treeRight)
            this.treeArray[v] = array[treeLeft];
        else {
            int tm = (treeLeft + treeRight) / 2;
            build (array, v*2, treeLeft, tm);
            build (array, v*2+1, tm+1, treeRight);
            treeArray[v] = treeArray[v*2] + treeArray[v*2+1];
        }
    }

    int sum (int v, int treeLeft, int treeRight, int l, int r) {
        if (l > r)
            return 0;
        if (l == treeLeft && r == treeRight)
            return treeArray[v];
        int tm = (treeLeft + treeRight) / 2;
        return sum (v*2, treeLeft, tm, l, min(r,tm))
                + sum (v*2+1, tm+1, treeRight, max(l,tm+1), r);
    }

    private int max(int a, int b) {
        return a > b ? a : b;
    }

    private int min(int a, int b) {
        return a < b ? a : b;
    }

    public void update(int pos, int x) {
        this.array[pos] = x;
        build(this.array, 1, 0, this.n - 1); 
    }

    public void add(int pos, int x) {
        int[] newArr = new int[this.array.length + 1];
        int i;
        for (i = 0; i < pos; i++) {
            newArr[i] = this.array[i];
        }
        newArr[i] = x;
        for (i += 1; i < newArr.length; i++) {
            newArr[i] = this.array[i - 1];
        }
        this.array = newArr;
        this.n = this.array.length;
        build(this.array, 1, 0, this.n - 1);
    }

    public void removeByIndex(int pos) {
        int[] newArr = new int[this.array.length - 1];
        int i;
        for(i = 0; i < pos; i++) {
            newArr[i] = this.array[i];
        }
        for(i += 1; i < this.array.length; i++) {
            newArr[i - 1] = this.array[i];
        }
        this.array = newArr;
        this.n = this.array.length;
        build(this.array, 1, 0, this.n - 1);
    }
}

// 0 1 2 3 4

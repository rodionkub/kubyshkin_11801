package ControlWorkNo2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        Stream actors = Files.lines(Paths.get("src/ControlWorkNo2/actors"));
        Stream directors = Files.lines(Paths.get("src/ControlWorkNo2/directors"));
        Stream films = Files.lines(Paths.get("src/ControlWorkNo2/films"));
        Stream appearances = Files.lines(Paths.get("src/ControlWorkNo2/appearances"));

        ArrayList appearancesList = new ArrayList();


        // Task #1
        films.filter(line -> line.toString().split(",")[0].equals("1")
                        && Integer.parseInt(line.toString().split(",")[2]) <= 2016)
                .forEach(System.out::println);
        films = Files.lines(Paths.get("src/ControlWorkNo2/films"));


        // Task #2
        ArrayList<Integer[]> actorAndFilmIds = new ArrayList<>();
        appearances.forEach(line -> actorAndFilmIds.add(new Integer[] {
                Integer.valueOf(line.toString().split(",")[1]),
                Integer.valueOf(line.toString().split(",")[0])
                }
                ));
        ArrayList<Integer[]> directorAndFilmIds = new ArrayList<>();
        films.forEach(line -> directorAndFilmIds.add(new Integer[] {
                Integer.parseInt(line.toString().split(",")[0]),
                Integer.parseInt(line.toString().split(",")[3])}
                ));

        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < actorAndFilmIds.size(); i++) {
            for (int j = 0; j < directorAndFilmIds.size(); j++) {
                if (Integer.parseInt(actorAndFilmIds.get(i)[1].toString()) == Integer.parseInt(directorAndFilmIds.get(j)[1].toString())) {
                    result.add(actorAndFilmIds.get(i)[0].toString() + "-" + directorAndFilmIds.get(j)[0].toString());
                }
            }
        }
        for (int i = 0; i < result.size(); i++) {
            int count = 1;
            for (int j = i + 1; j < result.size(); j++) {
                if (result.get(i).equals(result.get(j))) {
                    count++;
                    result.remove(j);
                }
            }
            String temp = "Actor ID: " + result.get(i).split("-")[0] + ", Director ID: " + result.get(i).split("-")[1] + ", Times worked together: " + count;
            result.remove(i);
            result.add(i, temp);
            System.out.println(result.get(i));
        }
    }

}
